#!/bin/bash
src_name=$(pwd)
px4_sitl="${src_name}/build/px4_sitl_default"
echo "Launch with bash setup_roslaunch.sh"
echo "Source is from ${src_name}"
DONT_RUN=1 make px4_sitl gazebo
source Tools/setup_gazebo.bash ${src_name} ${px4_sitl}
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:${src_name}
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:${src_name}/Tools/sitl_gazebo
echo "Roslaunch PX4"
