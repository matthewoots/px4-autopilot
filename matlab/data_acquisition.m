function [attitude_arr,controls_arr,acctuator_arr,acceleration_arr,angularRate_arr,velocity_arr] = data_acquisition(locationstr)
% attitude_arr
% controls_arr
% acctuator_arr
% acceleration_arr
% angularRate_arr
% velocity_arr
%% Data Acquisition
% https://www.mathworks.com/matlabcentral/fileexchange/23573-csvimport

% Download pyulog repo from GitHub
%     git clone https://github.com/PX4/pyulog
%     cd pyulog
%     python3 setup.py build install
%     ln -s ~/PX4-Autopilot/build/px4_sitl_default/logs ~/pyulog/

% If using logger session (set rate to 50):/home/nvidia/pyulog/logs/sess001/log001.ulg
% In Mavlink Terminal:
%% For Testing Longitudinal
%     logger stop (dont log initially) 
%     sysid startlong
%     logger start -r 10 
%     sysid pulse p 100 1000
%     logger stop

%% For Testing Lateral
%     logger stop (dont log initially) 
%     sysid startlat
%     logger start -r 10 
%     sysid pulse r 100 1000
%     logger stop

%% ulog to csv
% In Terminal:
%     ulog2csv -m vehicle_local_position,actuator_controls_0,actuator_outputs,vehicle_acceleration,vehicle_attitude,vehicle_angular_velocity logs/sess001/log001.ulg \
%     -o ~/PX4-Autopilot/matlab/testdata
% or if you want to download all topics (Not advisable) ulog2csv logs/2021-04-08/02_19_44.ulg

%% Parameters
% INPUTS:
% TOPIC: actuator_controls_0 (actuator_controls_s) control: [-0.5030, 1.0000, 0.0840, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000]
% TOPIC: actuator_outputs (actuator_outputs_s) output: [900.0000, 900.0000, 900.0000, 900.0000, 900.0000, 900.0000, 900.0000, 900.0000, 900.0000, 900.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000]

% STATES: etc @ 1 interval
% TOPIC: vehicle_acceleration (vehicle_acceleration_s) xyz: [-0.0549, -0.0757, -9.7980]
% TOPIC: vehicle_attitude (vehicle_attitude_s) (Roll: 0.2 deg, Pitch: -0.4 deg, Yaw: 89.8 deg)
% TOPIC: vehicle_angular_velocity (vehicle_angular_velocity_s) xyz: [0.0021, -0.0004, 0.0033]
% TOPIC: vehicle_local_position vehicle_local_position_s vx: -0.0052 vy: 0.0218 vz: -0.0324

%% Receive Paramters from PX4 
% location to retrieve the files
% locationstr = "testdata/";
% start by a pre string etc "09_10_25" or "log001"
prestr = "log001";
% usually followed by a post string
poststr = "_0";
input_parameters = ["_actuator_controls_0" "_actuator_outputs"];
states_parameters = ["_vehicle_acceleration" "_vehicle_attitude" "_vehicle_angular_velocity" "_vehicle_local_position"];

inputname = append(prestr,input_parameters,poststr,'.csv');
statesname = append(prestr,states_parameters,poststr,'.csv');

inputname = append(locationstr,inputname);
statesname = append(locationstr,statesname);

% For controls output
opts_c = detectImportOptions(inputname(1));
opts_c = setvartype(opts_c,{'timestamp','control_0_','control_1_','control_2_','control_3_'},'double');
opts_c.SelectedVariableNames = {'timestamp','control_0_','control_1_','control_2_','control_3_'};

% For actuator output
opts_a = detectImportOptions(inputname(2));
opts_a = setvartype(opts_a,{'timestamp','output_1_','output_2_','output_3_','output_4_','output_5_','output_6_','output_7_'},'double');
opts_a.SelectedVariableNames = {'timestamp','output_1_','output_2_','output_3_','output_4_','output_5_','output_6_','output_7_'};

% For acc
opts_ac = detectImportOptions(statesname(1));
opts_ac = setvartype(opts_ac,{'timestamp','xyz_0_','xyz_1_','xyz_2_'},'double');
opts_ac.SelectedVariableNames = {'timestamp','xyz_0_','xyz_1_','xyz_2_'};

% For att
opts_at = detectImportOptions(statesname(2));
opts_at = setvartype(opts_at,{'timestamp','q_0_','q_1_','q_2_','q_3_'},'double');
opts_at.SelectedVariableNames = {'timestamp','q_0_','q_1_','q_2_','q_3_'};

% For ang_vel
opts_av = detectImportOptions(statesname(3));
opts_av = setvartype(opts_av,{'timestamp','xyz_0_','xyz_1_','xyz_2_'},'double');
opts_av.SelectedVariableNames = {'timestamp','xyz_0_','xyz_1_','xyz_2_'};

% For velocity
opts_v = detectImportOptions(statesname(4));
opts_v = setvartype(opts_v,{'timestamp','vx','vy','vz','heading'},'double');
opts_v.SelectedVariableNames = {'timestamp','vx','vy','vz','heading'};

velocity_table = readtable(statesname(4),opts_v);

controls_table = readtable(inputname(1),opts_c);
actuator_table = readtable(inputname(2),opts_a);

acc_table = readtable(statesname(1),opts_ac);
attitude_table = readtable(statesname(2),opts_at);
angular_vel_table = readtable(statesname(3),opts_av);

temp_quad = table2array(attitude_table);
for i = 1:height(attitude_table)
    quat = quaternion(temp_quad(i,2:end));
    eulerYRP(i,:) = euler(quat,'ZYX','frame');
    % Conversion to RPY
    attitude_arr(i,1)=temp_quad(i,1);
    attitude_arr(i,2:4)=[eulerYRP(i,3),eulerYRP(i,2),eulerYRP(i,1)];
end

temp_velocity = table2array(velocity_table); % For rotational correction of velocity with body frame
for i = 1:height(velocity_table)
    velocity = [temp_velocity(i,2) ; temp_velocity(i,3)];
    rad = temp_velocity(i,5);
    Rinv = inv([cos(rad) -sin(rad);sin(rad) cos(rad)]);
    new_velocity(:,i) = Rinv * velocity; % X and Y are now local frame instead of global which helps in sysid
end

controls_arr = table2array(controls_table);
acctuator_arr = table2array(actuator_table);

acceleration_arr = table2array(acc_table);
angularRate_arr = table2array(angular_vel_table);
velocity_arr = table2array(velocity_table);

% Transfer velocity about local frame
velocity_arr(:,2) = transpose(new_velocity(1,:));
velocity_arr(:,3) = transpose(new_velocity(2,:));

figure
tiledlayout(2,1)
nexttile
plot(controls_arr(:,1),controls_arr(:,2)); 
hold on
plot(controls_arr(:,1),controls_arr(:,3)); 
plot(controls_arr(:,1),controls_arr(:,4)); 
plot(controls_arr(:,1),controls_arr(:,5)); 
title('Control output')
xlabel('timestamp')
ylabel('output')
legend('roll0','pitch1','yaw2','throttle3')
hold off
nexttile

plot(acctuator_arr(:,1),acctuator_arr(:,2)); 
hold on
plot(acctuator_arr(:,1),acctuator_arr(:,3)); 
plot(acctuator_arr(:,1),acctuator_arr(:,4)); 
plot(acctuator_arr(:,1),acctuator_arr(:,5)); 
plot(acctuator_arr(:,1),acctuator_arr(:,6)); 
plot(acctuator_arr(:,1),acctuator_arr(:,7)); 
plot(acctuator_arr(:,1),acctuator_arr(:,8));
title('Actuator output')
xlabel('timestamp')
ylabel('pwm')
legend('1','2','3','4','5','6','7','8')
hold off

figure 
tiledlayout(2,2)
nexttile
plot(acceleration_arr(:,1),acceleration_arr(:,2)); 
hold on
plot(acceleration_arr(:,1),acceleration_arr(:,3)); 
plot(acceleration_arr(:,1),acceleration_arr(:,4)); 
title('Acceleration')
xlabel('timestamp')
ylabel('acceleration m/s^2')
legend('X','Y','Z')
hold off
nexttile

plot(attitude_arr(:,1),attitude_arr(:,2)); 
hold on
plot(attitude_arr(:,1),attitude_arr(:,3)); 
plot(attitude_arr(:,1),attitude_arr(:,4)); 
title('Attitude')
xlabel('timestamp')
ylabel('RPY rad')
legend('roll','pitch','yaw')
hold off
nexttile

plot(angularRate_arr(:,1),angularRate_arr(:,2)); 
hold on
plot(angularRate_arr(:,1),angularRate_arr(:,3)); 
plot(angularRate_arr(:,1),angularRate_arr(:,4)); 
title('Angular Rate')
xlabel('timestamp')
ylabel('rad/s')
legend('p','q','r')
hold off
nexttile

plot(velocity_arr(:,1),velocity_arr(:,2)); 
hold on
plot(velocity_arr(:,1),velocity_arr(:,3)); 
plot(velocity_arr(:,1),velocity_arr(:,4)); 
title('Velocity')
xlabel('timestamp')
ylabel('m/s')
legend('X','Y','Z')
hold off

% [roll pitch yaw thrust]
end