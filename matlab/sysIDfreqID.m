%% Data Acquisition
% https://www.mathworks.com/matlabcentral/fileexchange/23573-csvimport

% Download pyulog repo from GitHub
%     git clone https://github.com/PX4/pyulog
%     cd pyulog
%     python3 setup.py build install
%     ln -s ~/PX4-Autopilot/build/px4_sitl_default/logs ~/pyulog/

% If using logger session (set rate to 50):/home/nvidia/pyulog/logs/sess001/log001.ulg
% In Mavlink Terminal:
%     logger stop (dont log initially) 
%     logger start -r 10 
%     pulse 20 pitch 1200
%     pulse 20 roll 1300
%     pulse 20 yaw 1300
%     logger stop
% In Terminal:
%     ulog2csv -m vehicle_local_position,actuator_controls_0,actuator_outputs,vehicle_acceleration,vehicle_attitude,vehicle_angular_velocity logs/sess001/log001.ulg \
%     -o ~/PX4-Autopilot/matlab/testdata
% or if you want to download all topics (Not advisable) ulog2csv logs/2021-04-08/02_19_44.ulg

%% Parameters
% INPUTS:
% TOPIC: actuator_controls_0 (actuator_controls_s) control: [-0.5030, 1.0000, 0.0840, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000]
% TOPIC: actuator_outputs (actuator_outputs_s) output: [900.0000, 900.0000, 900.0000, 900.0000, 900.0000, 900.0000, 900.0000, 900.0000, 900.0000, 900.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000]

% STATES: etc @ 1 interval
% TOPIC: vehicle_acceleration (vehicle_acceleration_s) xyz: [-0.0549, -0.0757, -9.7980]
% TOPIC: vehicle_attitude (vehicle_attitude_s) (Roll: 0.2 deg, Pitch: -0.4 deg, Yaw: 89.8 deg)
% TOPIC: vehicle_angular_velocity (vehicle_angular_velocity_s) xyz: [0.0021, -0.0004, 0.0033]
% TOPIC: vehicle_local_position vehicle_local_position_s vx: -0.0052 vy: 0.0218 vz: -0.0324

%% Receive Paramters from PX4 
clear 
clear all
close all
% % location to retrieve the files
% % locationstr = "testdata/";
% % start by a pre string etc "09_10_25" or "log001"
% prestr = "log001";
% % usually followed by a post string
% poststr = "_0";
% input_parameters = ["_actuator_controls_0" "_actuator_outputs"];
% states_parameters = ["_vehicle_acceleration" "_vehicle_attitude" "_vehicle_angular_velocity" "_vehicle_local_position"];
% 
% inputname = append(prestr,input_parameters,poststr,'.csv');
% statesname = append(prestr,states_parameters,poststr,'.csv');
% 
% inputname = append(locationstr,inputname);
% statesname = append(locationstr,statesname);
% 
% % For controls output
% opts_c = detectImportOptions(inputname(1));
% opts_c = setvartype(opts_c,{'timestamp','control_0_','control_1_','control_2_','control_3_'},'double');
% opts_c.SelectedVariableNames = {'timestamp','control_0_','control_1_','control_2_','control_3_'};
% 
% % For actuator output
% opts_a = detectImportOptions(inputname(2));
% opts_a = setvartype(opts_a,{'timestamp','output_1_','output_2_','output_3_','output_4_','output_5_','output_6_','output_7_'},'double');
% opts_a.SelectedVariableNames = {'timestamp','output_1_','output_2_','output_3_','output_4_','output_5_','output_6_','output_7_'};
% 
% % For acc
% opts_ac = detectImportOptions(statesname(1));
% opts_ac = setvartype(opts_ac,{'timestamp','xyz_0_','xyz_1_','xyz_2_'},'double');
% opts_ac.SelectedVariableNames = {'timestamp','xyz_0_','xyz_1_','xyz_2_'};
% 
% % For att
% opts_at = detectImportOptions(statesname(2));
% opts_at = setvartype(opts_at,{'timestamp','q_0_','q_1_','q_2_','q_3_'},'double');
% opts_at.SelectedVariableNames = {'timestamp','q_0_','q_1_','q_2_','q_3_'};
% 
% % For ang_vel
% opts_av = detectImportOptions(statesname(3));
% opts_av = setvartype(opts_av,{'timestamp','xyz_0_','xyz_1_','xyz_2_'},'double');
% opts_av.SelectedVariableNames = {'timestamp','xyz_0_','xyz_1_','xyz_2_'};
% 
% % For velocity
% opts_v = detectImportOptions(statesname(4));
% opts_v = setvartype(opts_v,{'timestamp','vx','vy','vz'},'double');
% opts_v.SelectedVariableNames = {'timestamp','vx','vy','vz'};
% 
% velocity_table = readtable(statesname(4),opts_v);
% 
% controls_table = readtable(inputname(1),opts_c);
% actuator_table = readtable(inputname(2),opts_a);
% 
% acc_table = readtable(statesname(1),opts_ac);
% attitude_table = readtable(statesname(2),opts_at);
% angular_vel_table = readtable(statesname(3),opts_av);
% 
% temp_quad = table2array(attitude_table);
% for i = 1:height(attitude_table)
%     quat = quaternion(temp_quad(i,2:end));
%     eulerYRP(i,:) = eulerd(quat,'ZYX','frame');
%     % Conversion to RPY
%     attitude_arr(i,1)=temp_quad(i,1);
%     attitude_arr(i,2:4)=[eulerYRP(i,3),eulerYRP(i,2),eulerYRP(i,1)];
% end
% 
% controls_arr = table2array(controls_table);
% acctuator_arr = table2array(actuator_table);
% 
% acceleration_arr = table2array(acc_table);
% angularRate_arr = table2array(angular_vel_table);
% velocity_arr = table2array(velocity_table);
% 
% figure
% tiledlayout(2,1)
% nexttile
% plot(controls_arr(:,1),controls_arr(:,2)); 
% hold on
% plot(controls_arr(:,1),controls_arr(:,3)); 
% plot(controls_arr(:,1),controls_arr(:,4)); 
% plot(controls_arr(:,1),controls_arr(:,5)); 
% title('Control output')
% xlabel('timestamp')
% ylabel('output')
% legend('roll0','pitch1','yaw2','throttle3')
% hold off
% nexttile
% 
% plot(acctuator_arr(:,1),acctuator_arr(:,2)); 
% hold on
% plot(acctuator_arr(:,1),acctuator_arr(:,3)); 
% plot(acctuator_arr(:,1),acctuator_arr(:,4)); 
% plot(acctuator_arr(:,1),acctuator_arr(:,5)); 
% plot(acctuator_arr(:,1),acctuator_arr(:,6)); 
% plot(acctuator_arr(:,1),acctuator_arr(:,7)); 
% plot(acctuator_arr(:,1),acctuator_arr(:,8));
% title('Actuator output')
% xlabel('timestamp')
% ylabel('pwm')
% legend('1','2','3','4','5','6','7','8')
% hold off
% 
% figure 
% tiledlayout(2,2)
% nexttile
% plot(acceleration_arr(:,1),acceleration_arr(:,2)); 
% hold on
% plot(acceleration_arr(:,1),acceleration_arr(:,3)); 
% plot(acceleration_arr(:,1),acceleration_arr(:,4)); 
% title('Acceleration')
% xlabel('timestamp')
% ylabel('acceleration m/s^2')
% legend('X','Y','Z')
% hold off
% nexttile
% 
% plot(attitude_arr(:,1),attitude_arr(:,2)); 
% hold on
% plot(attitude_arr(:,1),attitude_arr(:,3)); 
% plot(attitude_arr(:,1),attitude_arr(:,4)); 
% title('Attitude')
% xlabel('timestamp')
% ylabel('RPY deg')
% legend('roll','pitch','yaw')
% hold off
% nexttile
% 
% plot(angularRate_arr(:,1),angularRate_arr(:,2)); 
% hold on
% plot(angularRate_arr(:,1),angularRate_arr(:,3)); 
% plot(angularRate_arr(:,1),angularRate_arr(:,4)); 
% title('Angular Rate')
% xlabel('timestamp')
% ylabel('deg/s')
% legend('p','q','r')
% hold off
% nexttile
% 
% plot(velocity_arr(:,1),velocity_arr(:,2)); 
% hold on
% plot(velocity_arr(:,1),velocity_arr(:,3)); 
% plot(velocity_arr(:,1),velocity_arr(:,4)); 
% title('Velocity')
% xlabel('timestamp')
% ylabel('m/s')
% legend('X','Y','Z')
% hold off
% 
% % [roll pitch yaw thrust]

%% Main Code
% fft uses i and j. then we need i1,j1 for integer
% A matrix should be defined by the number of state input in longitudinal and lateral axis
% We will try Longitudinal here
A=[-0.6 0.95;-4.3 -1.2];


[m,n]=size(A);%mxn=4x4
% m=4;
% n=4;

B=[-0.115;-5.157];
[p,q]=size(B);%control variables pxq=4x2

% p=4;
% q=2;

%identifying first row  -0.6 0.95 -0.115  
% -0.5513
%     0.9714
%    -0.0518
%identifying second row -4.3 -1.2 -5.157
%  -4.3609
%    -1.0807
%    -5.1078

%sampling time interval and  points number
sampleT=0.025;%sampling interval second(unit)
Ns=600; %sample number
maxFreq=1.5;%maximum frequency 1.5Hz (unit)


%define  time interval, where we choose 15 seconds
timeInd=sampleT:sampleT:Ns*sampleT;

% we are concerning about 0.1-1.5hz. Thus 2hz crosses at t=7
% second,i.e.,we have maximum frequency at half time of the whole interval
u{1} = chirp(timeInd,0,7,2);%input signal delta_e,
%u{1}
% u{2}----- %delta_T
%plot

% u = input signal (pwm) delta_e
% compute fft of  q dimension of input
for  i1=1:q
    
       inputU(i1,:)=u{i1};
       fftu=fft(inputU(i1,:));
       fftU(i1,:)=fftu;

end

%sample state x value
x=zeros(m,Ns);
for iSample=1:Ns
    
    x(:,iSample+1)=x(:,iSample)+sampleT*(A*x(:,iSample)+B*inputU(:,iSample));   

end
% x(1,:)
% plot

% x(1,:)=
% x(2,:)
% x(3,:)
% x(4,:)


%compute x's fft 
freqL=length(x(1,:));
fftX=zeros(m,freqL);
for xInd=1:m
    
        fftX(xInd,:)=fft(x(xInd,:));
    
end

%%%%plot u and x
figure (1)
subplot(2,1,1)
plot(timeInd,u{1})
ylabel('Control signal');

subplot(2,1,2)
Ts=sampleT;
fs = 1/Ts;
fftLength=length(fftU(1,:));
f = (0:length(fftU(1,:))-1)*fs/fftLength;
plot(f,abs(fftU(1,:)))
xlabel('Input Frequency (Hz)')
ylabel('Mag')


figure (2)
subplot(2,2,1)
plot(1:Ns,x(1,1:Ns),'*')
ylabel('x1')

subplot(2,2,2)
fftX1length=length(fftX(1,:));
fx1 = (0:fftX1length-1)*fs/fftX1length;
plot(fx1,abs(fftX(1,:)  ))
xlabel('Frequency (Hz)')
ylabel('Mag')
subplot(2,2,3)
plot(1:Ns,x(2,1:Ns),'*')
ylabel('x2')
subplot(2,2,4)
fftX2length=length(fftX(2,:));
fx2 = (0:fftX2length-1)*fs/fftX2length;
plot(fx2,abs(fftX(2,:) ))
xlabel('Frequency (Hz)')
ylabel('Mag')

% %compute x's fft 
% freqL=length(x(1,:));
% fftX=zeros(m,freqL);
% for xInd=1:m
%     
%         fftX(xInd,:)=fft(x(xInd,:));
%     
% end
% fftX1=fft(x(1,:));
% fftX2=fft(x(2,:));
% length(fftX1)

% fftX(1,1)
% fftX(2,1)
%compute fft of U
% fftU=zeros(q,freqL);
% for uInd=1:q
%     for omegaJ=1:freqL
%         knU=0;
%         for h=1:Ns
%             
%             knU=knU+u(uInd,h)*exp(-i*2*pi*fPts(omegaJ)*h);
%             
%         end
%         fftU(uInd,omegaJ)=knU*sampleT;
%     end
% end

%finding maximum index of interesting frequency bound of [0.1-1.5]
interestFreqMaxInd=1;
for fInd=1:freqL
   if fx1(fInd)>maxFreq
       interestFreqMaxInd=fInd-1;
       break;
   end
    
end

for xInd=1:m
    lsqY=[];
    for omegaJ=1:interestFreqMaxInd
        omegaAngle=(2*pi*fx1(omegaJ));
        lsqY=[lsqY;j*omegaAngle*fftX(xInd,omegaJ)];%fftX(1,omegaJ) identify the first row,fftX(2,omegaJ) secondrow
    end

    lsqX=[];
    for omegaJ=1:interestFreqMaxInd
         lsqX=[lsqX; fftX(:,omegaJ).' fftU(:,omegaJ).'];
    end

    % least square ID
     estB(xInd,:)=inv(real(lsqX'*(lsqX)))*real(lsqX'*lsqY);
     
end
%display all parameters according to the row list
'Estimated Parmaters (row list)' 
estB
