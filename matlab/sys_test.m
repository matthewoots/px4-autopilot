%% Receive Paramters from PX4 
clear 
clear all
close all
% location to retrieve the files
% Change Longitudinal and Lateral
testlong = false; % else test lat
if testlong
    locationstr = "testlogs_long/";
else
    locationstr = "testlogs_lat/";
end

[attitude_arr,controls_arr,acctuator_arr,acceleration_arr,angularRate_arr,velocity_arr] = data_acquisition(locationstr);

%% Main Code
% fft uses i and j. then we need i1,j1 for integer
% A matrix should be defined by the number of state input in longitudinal and lateral axis
% We will try Longitudinal here 
A = zeros(4,4);
[m , n] = size(A);

if testlong
    B = zeros(4,1);
else
    B = zeros(4,2);
end

[p , q] = size(B); % control variables p x q = 4 x 1 no need for throttle

% sampling time interval and  points number
loggingHz = 10; % determind from PX4 logger
sampleT = 1 / loggingHz; % sampling interval second (unit)
Ns = height(attitude_arr); % sample number
maxFreq = 1.5; % maximum frequency 1.5Hz (unit)
% define  time interval, where we choose what the pixhawk gives us
timeInd = sampleT:sampleT:Ns * sampleT;

%% FFT
% u = input signal (pwm) delta_e
factor_rad = deg2rad(50) / 2; % -1 to 1
if testlong
    q = 1; % for longitudinal 
    trim_value_pitch = controls_arr(1,3); % level flight
    u{1} = (transpose(controls_arr(:,3)) - trim_value_pitch) * factor_rad ; 
    
    % Feed in state for longitudinal value (u w q theta(pitch))
    x_name = ['u','w','q','theta'];
    x(1,:) = transpose(velocity_arr(:,2)) - velocity_arr(1,2);
    x(2,:) = transpose(velocity_arr(:,4) - velocity_arr(1,4));
    x(3,:) = transpose(angularRate_arr(:,3) - angularRate_arr(1,3));
    x(4,:) = transpose(attitude_arr(:,3) - attitude_arr(1,3));
    load('longitudinal.mat','sysA');
    load('longitudinal.mat','sysB');

else 
    q = 2; % for lateral
    trim_value_roll = controls_arr(1,2); % level flight
    trim_value_yaw = controls_arr(1,4); % level flight
    u{1} = transpose(controls_arr(:,2) - trim_value_roll) * factor_rad ;
    u{2} = transpose(controls_arr(:,4) - trim_value_yaw) * factor_rad ; 
    
    % Feed in state for longitudinal value (v p r phi(roll))
    x_name = ['v','p','r','phi'];
    x(1,:) = transpose(velocity_arr(:,3)) - velocity_arr(1,3);
    x(2,:) = transpose(angularRate_arr(:,2)) - angularRate_arr(1,2);
    x(3,:) = transpose(angularRate_arr(:,4)) - angularRate_arr(1,4);
    x(4,:) = transpose(attitude_arr(:,2)) - attitude_arr(1,2);
    load('lateral.mat','sysA');
    load('lateral.mat','sysB');
end

for i = 1:q
    input_u(i, :) = u{i};
end

sample_start = 1;
end_sample = width(timeInd);
test_x = zeros(m, end_sample - sample_start);

test_x(:,sample_start) = x(:,sample_start); % get initial state
for i = sample_start+1:end_sample
    test_x_A(:,i) = sysA * test_x(:,i-1);
    test_x_B(:,i) = sysB * input_u(:,i-1);
    test_x(:,i) = test_x_A(:,i) + test_x_B(:,i);
end

figure (3) 
subplot(m,1,1)
plot(timeInd(1,:),x(1,:))
hold on 
% plot(timeInd(1,1:50),test_x(1,1:50))
hold off

sample_end = 78;
figure (4)
subplot(4,1,1)
plot(timeInd(1,sample_start:sample_end),test_x_A(1,sample_start:sample_end))
subplot(4,1,2)
plot(timeInd(1,sample_start:sample_end),test_x_A(2,sample_start:sample_end))
subplot(4,1,3)
plot(timeInd(1,sample_start:sample_end),test_x_A(3,sample_start:sample_end))
subplot(4,1,4)
plot(timeInd(1,sample_start:sample_end),test_x_A(4,sample_start:sample_end))

figure (5)
subplot(4,1,1)
plot(timeInd(1,sample_start:sample_end),test_x_B(1,sample_start:sample_end))
subplot(4,1,2)
plot(timeInd(1,sample_start:sample_end),test_x_B(2,sample_start:sample_end))
subplot(4,1,3)
plot(timeInd(1,sample_start:sample_end),test_x_B(3,sample_start:sample_end))
subplot(4,1,4)
plot(timeInd(1,sample_start:sample_end),test_x_B(4,sample_start:sample_end))

% ylabel(x_name(1))
% subplot(m,2,2)
% fftX1length=length(fftX(1,:));
% fx1 = (0:fftX1length-1)*fs/fftX1length;
% plot(fx1,abs(fftX(1,:)  ))
% xlabel('Frequency (Hz)')
% ylabel('Mag')
% 
% subplot(m,2,3)
% plot(1:Ns,x(2,1:Ns),'*')
% ylabel(x_name(2))
% subplot(m,2,4)
% fftX2length=length(fftX(2,:));
% fx2 = (0:fftX2length-1)*fs/fftX2length;
% plot(fx2,abs(fftX(2,:) ))
% xlabel('Frequency (Hz)')
% ylabel('Mag')
% 
% subplot(m,2,5)
% plot(1:Ns,x(3,1:Ns),'*')
% ylabel(x_name(3))
% subplot(m,2,6)
% fftX3length=length(fftX(3,:));
% fx3 = (0:fftX3length-1)*fs/fftX3length;
% plot(fx3,abs(fftX(3,:) ))
% xlabel('Frequency (Hz)')
% ylabel('Mag')
% 
% subplot(m,2,7)
% plot(1:Ns,x(4,1:Ns),'*')
% ylabel(x_name(4))
% subplot(m,2,8)
% fftX4length=length(fftX(4,:));
% fx4 = (0:fftX4length-1)*fs/fftX4length;
% plot(fx4,abs(fftX(4,:) ))
% xlabel('Frequency (Hz)')
% ylabel('Mag')
