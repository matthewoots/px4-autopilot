%% Receive Paramters from PX4 
clear 
clear all
close all
% location to retrieve the files
% Change Longitudinal and Lateral
testlong = false; % else test lat
if testlong
    locationstr = "testdata_long/";
else
    locationstr = "testdata_lat/";
end

[attitude_arr,controls_arr,acctuator_arr,acceleration_arr,angularRate_arr,velocity_arr] = data_acquisition(locationstr);

%% Main Code
% fft uses i and j. then we need i1,j1 for integer
% A matrix should be defined by the number of state input in longitudinal and lateral axis
% We will try Longitudinal here 
A = zeros(4,4);
[m , n] = size(A);

if testlong
    B = zeros(4,1);
else
    B = zeros(4,2);
end

[p , q] = size(B); % control variables p x q = 4 x 1 no need for throttle

% sampling time interval and  points number
loggingHz = 10; % determind from PX4 logger
sampleT = 1 / loggingHz; % sampling interval second (unit)
Ns = height(attitude_arr); % sample number
maxFreq = 1.5; % maximum frequency 1.5Hz (unit)
% define  time interval, where we choose what the pixhawk gives us
timeInd = sampleT:sampleT:Ns * sampleT;

%% FFT
% u = input signal (pwm) delta_e
factor_rad = deg2rad(50) / 2; % -1 to 1
if testlong
    q = 1; % for longitudinal 
    trim_value_pitch = controls_arr(1,3); % level flight
    u{1} = (transpose(controls_arr(:,3)) - trim_value_pitch) * factor_rad ; 
    
    % Feed in state for longitudinal value (u w q theta(pitch))
    x_name = ['u','w','q','theta'];
    x(1,:) = transpose(velocity_arr(:,2)) - velocity_arr(1,2);
    x(2,:) = transpose(velocity_arr(:,4) - velocity_arr(1,4));
    x(3,:) = transpose(angularRate_arr(:,3) - angularRate_arr(1,3));
    x(4,:) = transpose(attitude_arr(:,3) - attitude_arr(1,3));
else 
    q = 2; % for lateral
    trim_value_roll = controls_arr(1,2); % level flight
    trim_value_yaw = controls_arr(1,4); % level flight
    u{1} = transpose(controls_arr(:,2) - trim_value_roll) * factor_rad ;
    u{2} = transpose(controls_arr(:,4) - trim_value_yaw) * factor_rad ; 
    
    % Feed in state for longitudinal value (v p r phi(roll))
    x_name = ['v','p','r','phi'];
    x(1,:) = transpose(velocity_arr(:,3)) - velocity_arr(1,3);
    x(2,:) = transpose(angularRate_arr(:,2)) - angularRate_arr(1,2);
    x(3,:) = transpose(angularRate_arr(:,4)) - angularRate_arr(1,4);
    x(4,:) = transpose(attitude_arr(:,2)) - attitude_arr(1,2);
end

% compute fft of  q dimension of input
for i = 1:q
    input_u(i, :) = u{i};
    fft_u = fft(input_u(i, :));
    fftU(i,:) = fft_u;
end

%compute x's fft 
freqL=length(x(1,:));
fftX=zeros(m,freqL);
for xInd=1:m
    
        fftX(xInd,:)=fft(x(xInd,:));
    
end

%% Plot of u and x
figure 
subplot(2,1,1)
plot(timeInd,u{1})
ylabel('Control signal');

subplot(2,1,2)
Ts = sampleT;
fs = 1/Ts;
fftLength = length(fftU(1,:));
f = (0:length(fftU(1,:))-1) * fs / fftLength;
plot(f, abs(fftU(1,:)))
xlabel('Input Frequency (Hz)')
ylabel('Mag')

subplot(2,1,2)
Ts=sampleT;
fs = 1/Ts;
fftLength=length(fftU(1,:));
f = (0:length(fftU(1,:))-1)*fs/fftLength;
plot(f,abs(fftU(1,:)))
xlabel('Input Frequency (Hz)')
ylabel('Mag')


figure
subplot(m,2,1)
plot(1:Ns,x(1,1:Ns),'*')
ylabel(x_name(1))
subplot(m,2,2)
fftX1length=length(fftX(1,:));
fx1 = (0:fftX1length-1)*fs/fftX1length;
plot(fx1,abs(fftX(1,:)  ))
xlabel('Frequency (Hz)')
ylabel('Mag')

subplot(m,2,3)
plot(1:Ns,x(2,1:Ns),'*')
ylabel(x_name(2))
subplot(m,2,4)
fftX2length=length(fftX(2,:));
fx2 = (0:fftX2length-1)*fs/fftX2length;
plot(fx2,abs(fftX(2,:) ))
xlabel('Frequency (Hz)')
ylabel('Mag')

subplot(m,2,5)
plot(1:Ns,x(3,1:Ns),'*')
ylabel(x_name(3))
subplot(m,2,6)
fftX3length=length(fftX(3,:));
fx3 = (0:fftX3length-1)*fs/fftX3length;
plot(fx3,abs(fftX(3,:) ))
xlabel('Frequency (Hz)')
ylabel('Mag')

subplot(m,2,7)
plot(1:Ns,x(4,1:Ns),'*')
ylabel(x_name(4))
subplot(m,2,8)
fftX4length=length(fftX(4,:));
fx4 = (0:fftX4length-1)*fs/fftX4length;
plot(fx4,abs(fftX(4,:) ))
xlabel('Frequency (Hz)')
ylabel('Mag')

%finding maximum index of interesting frequency bound of [0.1-1.5]
interestFreqMaxInd=1;
for fInd=1:freqL
   if fx1(fInd)>maxFreq
       interestFreqMaxInd=fInd-1;
       break;
   end
    
end

for xInd=1:m
    lsqY=[];
    for omegaJ=1:interestFreqMaxInd
        omegaAngle=(2*pi*fx1(omegaJ));
        lsqY=[lsqY;j*omegaAngle*fftX(xInd,omegaJ)];%fftX(1,omegaJ) identify the first row,fftX(2,omegaJ) secondrow
    end

    lsqX=[];
    for omegaJ=1:interestFreqMaxInd
         lsqX=[lsqX; fftX(:,omegaJ).' fftU(:,omegaJ).'];
    end

    % least square ID
     estB(xInd,:)=inv(real(lsqX'*(lsqX)))*real(lsqX'*lsqY);
     
end
% display all parameters according to the row list
'Estimated Parmaters (row list)' ;

sysA = estB(:,1:m);
sysB = estB(:,m+1:end);
eigenA = eig(sysA);
% e = eig(A,'matrix');
testlong

sys_discrete = ss(sysA,sysB,eye(size(sysA)),zeros(height(sysB),width(sysB)),sampleT);
sys_continous = d2c(sys_discrete,'foh');

eigenA_c = eig(sys_continous.A)
eigenA
