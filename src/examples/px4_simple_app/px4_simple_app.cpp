/****************************************************************************
 *
 *   Copyright (c) 2012-2019 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/**
 * @file px4_simple_app.c
 * Minimal application example for PX4 autopilot
 *
 * @author Example User <mail@example.com>
 */
#include <uORB/topics/actuator_outputs.h>
#include "simulator.h"
#include <drivers/drv_pwm_output.h>

#include <px4_platform_common/px4_config.h>
#include <px4_platform_common/tasks.h>
#include <px4_platform_common/posix.h>
#include <unistd.h>
#include <stdio.h>
#include <poll.h>
#include <string.h>
#include <math.h>

#include <uORB/uORB.h>
#include <uORB/topics/vehicle_acceleration.h>
#include <uORB/topics/vehicle_attitude.h>
#include <uORB/topics/actuator_outputs.h>


extern "C" __EXPORT int px4_simple_app_main(int argc, char *argv[]);

int px4_simple_app_main(int argc, char *argv[])
{
	PX4_INFO("PWM Testing!");
	uORB::Subscription _vehicle_status_sub{ORB_ID(vehicle_status)};
	vehicle_status_s vehicle_status;
	_vehicle_status_sub.update(&vehicle_status);


	/* subscribe to vehicle_acceleration topic */
	int sensor_sub_fd = orb_subscribe(ORB_ID(vehicle_acceleration));
	/* subscribe to actuator_outputs topic */
	int actuator_sub_fd = orb_subscribe(ORB_ID(actuator_outputs));
	/* limit the update rate to 5 Hz */
	orb_set_interval(sensor_sub_fd, 200);
	orb_set_interval(actuator_sub_fd, 200);


	/* advertise attitude topic */
	struct vehicle_attitude_s att;
	memset(&att, 0, sizeof(att));
	orb_advert_t att_pub = orb_advertise(ORB_ID(vehicle_attitude), &att);

	/* one could wait for multiple topics with this technique, just using one here */
	px4_pollfd_struct_t fds[] = {
		{ .fd = sensor_sub_fd,   .events = POLLIN },
		{ .fd = actuator_sub_fd,   .events = POLLIN },
		/* there could be more file descriptors here, in the form like:
		 * { .fd = other_sub_fd,   .events = POLLIN },
		 */
	};

	// actuator_outputs_s act = {};
	// mavlink_servo_output_raw_t msg{};

	// msg.time_usec = act.timestamp;
	// // msg.port = N;
	// msg.servo1_raw = act.output[0];
	// msg.servo2_raw = act.output[1];
	// msg.servo3_raw = act.output[2];
	// msg.servo4_raw = act.output[3];
	// msg.servo5_raw = act.output[4];
	// msg.servo6_raw = act.output[5];
	// msg.servo7_raw = act.output[6];
	// msg.servo8_raw = act.output[7];
	// msg.servo9_raw = act.output[8];
	// msg.servo10_raw = act.output[9];
	// msg.servo11_raw = act.output[10];
	// msg.servo12_raw = act.output[11];
	// msg.servo13_raw = act.output[12];
	// msg.servo14_raw = act.output[13];
	// msg.servo15_raw = act.output[14];
	// msg.servo16_raw = act.output[15];

	// mavlink_msg_servo_output_raw_send_struct(_mavlink->get_channel(), &msg);

	mavlink_hil_actuator_controls_t hil_act_control;
	int _actuator_outputs_sub{-1};
	actuator_outputs_s _actuator_outputs{};
	_actuator_outputs_sub = orb_subscribe_multi(ORB_ID(actuator_outputs), 0);

	orb_copy(ORB_ID(actuator_outputs), _actuator_outputs_sub, &_actuator_outputs);

	for (unsigned i = 0; i < actuator_outputs_s::NUM_ACTUATOR_OUTPUTS; i++) {
	hil_act_control.controls[i] = (_actuator_outputs.output[i] - PWM_DEFAULT_MIN) / (PWM_DEFAULT_MAX - PWM_DEFAULT_MIN);
	hil_act_control.controls[i] = math::constrain(hil_act_control.controls[i], 0.f, 1.f);
	}

	// actuator_controls_from_outputs(&hil_act_control);

	// mavlink_message_t message{};
	// mavlink_msg_hil_actuator_controls_encode(_param_mav_sys_id.get(), _param_mav_comp_id.get(), &message, &hil_act_control);

	// PX4_INFO("sending controls t=%ld (%ld)", _actuator_outputs.timestamp, hil_act_control.time_usec);

	// send_mavlink_message(message);


	int error_counter = 0;

	for (int i = 0; i < 5; i++) {
		/* wait for sensor update of 1 file descriptor for 1000 ms (1 second) */
		int poll_ret = px4_poll(fds, 1, 1000);

		/* handle the poll result */
		if (poll_ret == 0) {
			/* this means none of our providers is giving us data */
			PX4_ERR("Got no data within a second");

		} else if (poll_ret < 0) {
			/* this is seriously bad - should be an emergency */
			if (error_counter < 10 || error_counter % 50 == 0) {
				/* use a counter to prevent flooding (and slowing us down) */
				PX4_ERR("ERROR return value from poll(): %d", poll_ret);
			}

			error_counter++;

		} else {

			if (fds[0].revents & POLLIN) {
				/* obtained data for the first file descriptor */
				/* copy sensors raw data into local buffer */
				struct actuator_outputs_s act_output;
				orb_copy(ORB_ID(actuator_outputs), actuator_sub_fd, &act_output);
				PX4_INFO("Actuator:    Yaw: %.1f    Throttle: %.1f    Roll: %.1f    Roll: %.1f    Pitch: %.1f",
					 (double)act_output.output[2],
					 (double)act_output.output[4],
					 (double)act_output.output[5],
					 (double)act_output.output[6],
					 (double)act_output.output[7]);

				/* set att and publish this information for other apps
				 the following does not have any meaning, it's just an example
				*/
				struct vehicle_acceleration_s accel;
				/* copy sensors raw data into local buffer */
				orb_copy(ORB_ID(vehicle_acceleration), sensor_sub_fd, &accel);
				att.q[0] = accel.xyz[0];
				att.q[1] = accel.xyz[1];
				att.q[2] = accel.xyz[2];

				orb_publish(ORB_ID(vehicle_attitude), att_pub, &att);
			}

			/* there could be more file descriptors here, in the form like:
			 * if (fds[1..n].revents & POLLIN) {}
			 */
		}
	}

	PX4_INFO("exiting");

	return 0;
}
