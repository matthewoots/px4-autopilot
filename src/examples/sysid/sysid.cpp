/****************************************************************************
 *
 *   Copyright (c) 2012-2019 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/**
 * @file sysid.c
 * System ID application example for PX4 autopilot
 *
 * @author woots <mwoo@nus.edu.sg>
 */

#include <px4_platform_common/px4_config.h>
#include <px4_platform_common/tasks.h>
#include <px4_platform_common/posix.h>
#include <px4_platform_common/module.h>

#include <unistd.h>
#include <stdio.h>
#include <poll.h>
#include <string.h>
#include <math.h>

#include <uORB/uORB.h>
#include <uORB/topics/actuator_controls.h>
#include <uORB/topics/sys_id.h>
#include <uORB/topics/setstate.h>

#include <uORB/Publication.hpp>
#include <uORB/Subscription.hpp>
#include <uORB/SubscriptionInterval.hpp>


// #include <v2.0/common/mavlink.h>
#include <v2.0/mavlink_types.h>

#ifdef __PX4_NUTTX
#include <nuttx/fs/ioctl.h>
#endif

static void	usage(const char *reason);
__BEGIN_DECLS
__EXPORT void 	sysid_main(int argc, char *argv[]);
__EXPORT void 	reset();
__EXPORT void	modedecision(const char *arg);
__EXPORT void	trimtransfer();
__END_DECLS

extern "C" __EXPORT void sysid_main(int argc, char *argv[]);
extern "C" __EXPORT void reset();
extern "C" __EXPORT void modedecision(const char *arg);
extern "C" __EXPORT void trimtransfer();
extern "C" __EXPORT void msg_to_gazebo(bool s);

struct actuator_controls_s _actuator_controls;
struct sys_id_s _sys_id;
struct setstate_s _setstate;
int roll = actuator_controls_s::INDEX_ROLL;
int pitch = actuator_controls_s::INDEX_PITCH;
int yaw = actuator_controls_s::INDEX_YAW;
int throttle = actuator_controls_s::INDEX_THROTTLE;

static void
usage(const char *reason)
{
	if (reason != nullptr) {
		PX4_WARN("%s", reason);
	}

	PRINT_MODULE_DESCRIPTION(
		R"DESCR_STR(
		### Description
		This command is used to launch system identification mode, enabling start and stop of mode and pulse.
		)DESCR_STR");

	PRINT_MODULE_USAGE_NAME("sysid", "command");
	PRINT_MODULE_USAGE_COMMAND_DESCR("start", "Start System Identification (no controller mode)");
	PRINT_MODULE_USAGE_COMMAND_DESCR("stop", "Stop System Identification (no controller mode)");
	PRINT_MODULE_USAGE_COMMAND_DESCR("pulse", "Stop System Identification (no controller mode)");

	PRINT_MODULE_USAGE_PARAM_COMMENT("1st arg: 't' 'p' 'y' 'r'");
	PRINT_MODULE_USAGE_PARAM_COMMENT("2nd arg: Period (how long do you want the overwrite to be) it is in ms.");
	PRINT_MODULE_USAGE_PARAM_COMMENT("3rd arg: Value of control output. It is multiplied by 1000 for conveience, and scaled back down before output. Throttle: (0 to 1000), the rest: (-1000 to 1000)");

	PRINT_MODULE_USAGE_PARAM_COMMENT("eg sysid pulse t 10 400");

}

void sysid_main(int argc, char *argv[])
{
	if (argc < 2) {
		usage(nullptr);
		PX4_INFO("Incorrect Input, sysid [COMMAND] [OPTIONS]");
		return ;
	}

	PX4_INFO("#sysID Testing!");
	/* initialize parameters */

	const char *command = argv[1];
	if (strcmp(command, "startlong") && strcmp(command, "startlat") && strcmp(command, "stop") && strcmp(command, "pulse")) return;

	/* subscribe to actuator_control topic */
	int actuator_controls_sub_fd = orb_subscribe(ORB_ID(actuator_controls_0));
	/* limit the update rate to 100 Hz */
	orb_set_interval(actuator_controls_sub_fd, 100);
	/* obtained data for the first file descriptor */
	/* copy control raw data into local buffer */
	orb_copy(ORB_ID(actuator_controls_0), actuator_controls_sub_fd, &_actuator_controls);

	/* subscribe to sys_id topic */
	int sys_id_sub_fd = orb_subscribe(ORB_ID(sys_id));
	// /* limit the update rate to 100 Hz */
	orb_set_interval(sys_id_sub_fd, 100);
	/* obtained data for the first file descriptor */
	/* copy control raw data into local buffer */
	orb_copy(ORB_ID(sys_id), sys_id_sub_fd, &_sys_id);
	/* advertise to sys_id topic */
	orb_advert_t _sys_id_pub = orb_advertise(ORB_ID(sys_id), &_sys_id);

	if (!_sys_id.initialised){
		reset();
		_sys_id.initialised = true;
		// Initialise and publish
		orb_publish(ORB_ID(sys_id), _sys_id_pub, &_sys_id);
		PX4_INFO("#sysID Initialised");
	}
	else
	{
		// Publish to get a poll
		orb_publish(ORB_ID(sys_id), _sys_id_pub, &_sys_id);
		PX4_INFO("#sysID Already Initialised");
	}

	/* one could wait for multiple topics with this technique, just using one here */
	px4_pollfd_struct_t fds[] = {
		{ .fd = sys_id_sub_fd,   .events = POLLIN },
		{ .fd = actuator_controls_sub_fd,   .events = POLLIN },
		/* there could be more file descriptors here, in the form like:
		 * { .fd = other_sub_fd,   .events = POLLIN },
		 */
	};

	int error_counter = 0, threshold = 1;
	for (int i = 0; i < threshold; i++) {
		/* wait for sensor update of 1 file descriptor for 1000 ms */
		int poll_ret = px4_poll(fds, 1, 1000);

		/* handle the poll result */
		if (poll_ret == 0) {
			/* this means none of our providers is giving us data */
			PX4_ERR("Got no data within a second");

		} else if (poll_ret < 0) {
			/* this is seriously bad - should be an emergency */
			if (error_counter < 10 || error_counter % 50 == 0) {
				/* use a counter to prevent flooding (and slowing us down) */
				PX4_ERR("ERROR return value from poll(): %d", poll_ret);
			}

			error_counter++;

		} else {

			if (fds[0].revents & POLLIN) {
				uORB::Subscription _actuator_controls_sub{ORB_ID(actuator_controls_0)};
				_actuator_controls_sub.update(&_actuator_controls);

				if (!strcmp(command, "startlong")) {
					/* Return if already started */
					if (_sys_id.start_now)
					{
						PX4_INFO("#sysID Already started");
						return;
					}
					msg_to_gazebo(true);

					/* We take the initial state from control output */
					trimtransfer();
					_sys_id.start_now = true;
					_sys_id.state = sys_id_s::TEST_LONG_STATE;

					/* Publication of trim condition */
					orb_publish(ORB_ID(sys_id), _sys_id_pub, &_sys_id);
					PX4_INFO("#sysID Long Start");
				}

				if (!strcmp(command, "startlat")) {
					/* Return if already started */
					if (_sys_id.start_now)
					{
						PX4_INFO("#sysID Already started");
						return;
					}
					msg_to_gazebo(true);

					/* We take the initial state from control output */
					trimtransfer();
					_sys_id.start_now = true;
					_sys_id.state = sys_id_s::TEST_LAT_STATE;

					/* Publication of trim condition */
					orb_publish(ORB_ID(sys_id), _sys_id_pub, &_sys_id);
					PX4_INFO("#sysID Lat Start");
				}

				if (!strcmp(command, "stop")) {
					/* Return if not started */
					if (!_sys_id.start_now)	{
						PX4_INFO("#sysID Nothing to stop");
						reset();
						return;
					}
					/* Reset sys_id message */
					reset();
					_sys_id.initialised = false;

					/* Publication of reset condition */
					orb_publish(ORB_ID(sys_id), _sys_id_pub, &_sys_id);
					PX4_INFO("#sysID Stop");
				}

				if (!strcmp(command, "pulse")) {
					/* Check for type, amplitude and period */
					if (argc != 5) {
						PX4_INFO("#sysID Incorrect input for pulse");
						return ;
					}
					/* Check if sysid module has started */
					if (!_sys_id.start_now) {
						PX4_INFO("#sysID Module has not started");
						return ;
					}
					/* Check if pulse is started */
					if (_sys_id.activate) {
						PX4_INFO("#sysID Pulse already started");
						return ;
					}

					/* Get pulse period from argv[3] and amplitude from argv[4] */
					_sys_id.period = atoi(argv[3]);
					_sys_id.amplitude = atoi(argv[4])/1000;

					/* Check which controls to toggle */
					modedecision(argv[2]);

					_sys_id.activate = true;
					_sys_id.debug = true;
					_sys_id.start_time = (_actuator_controls.timestamp / 10e3);

					PX4_INFO("#sysid Pulse activation: %s", _sys_id.activate ? "true" : "false");

					/* Publication of reset condition */
					orb_publish(ORB_ID(sys_id), _sys_id_pub, &_sys_id);
				}
			}
			/* there could be more file descriptors here, in the form like:
			 * if (fds[1..n].revents & POLLIN) {}
			 */
		}
	}
	PX4_INFO("#Exiting");
	return;
}

void reset()
{

	_sys_id.start_now = false;
	_sys_id.activate = false;
	_sys_id.debug = false;

	_sys_id.amplitude = 0;
	_sys_id.start_time = 0;
	_sys_id.period = 0; // millisecond

	_sys_id.sendthrottle = false;
	_sys_id.sendroll = false;
	_sys_id.sendpitch = false;
	_sys_id.sendyaw = false;

	_sys_id.trim[throttle] = 0;
	_sys_id.trim[roll] = 0;
	_sys_id.trim[pitch] = 0;
	_sys_id.trim[yaw] = 0;

	_sys_id.state = 0;
	msg_to_gazebo(false);
	return;
}

void modedecision(const char *arg)
{
	/* Single */
	if (!strcmp(arg, "t")) {
		PX4_INFO("#Pushing throttle");
		_sys_id.sendthrottle = true;
	}
	if (!strcmp(arg, "r")) {
		PX4_INFO("#Pushing roll");
		_sys_id.sendroll = true;
	}
	if (!strcmp(arg, "p")) {
		PX4_INFO("#Pushing pitch");
		_sys_id.sendpitch = true;
	}
	if (!strcmp(arg, "y")) {
		PX4_INFO("#Pushing yaw");
		_sys_id.sendyaw = true;
	}

	/* Multiple */
	if (!strcmp(arg, "rp") || !strcmp(arg, "pr")) {
		PX4_INFO("#Pushing roll and pitch");
		_sys_id.sendroll = true;
		_sys_id.sendpitch = true;
	}
	if (!strcmp(arg, "py") || !strcmp(arg, "yp")) {
		PX4_INFO("#Pushing pitch and yaw");
		_sys_id.sendpitch = true;
		_sys_id.sendyaw = true;
	}
	if (!strcmp(arg, "ry") || !strcmp(arg, "yr")) {
		PX4_INFO("#Pushing roll and yaw");
		_sys_id.sendroll = true;
		_sys_id.sendyaw = true;
	}
	if (!strcmp(arg, "rpy")) {
		PX4_INFO("#Pushing roll, pitch and yaw");
		_sys_id.sendroll = true;
		_sys_id.sendpitch = true;
		_sys_id.sendyaw = true;
	}
	return;
}

void trimtransfer()
{
	_sys_id.trim[roll] = _actuator_controls.control[roll];
	_sys_id.trim[pitch] = _actuator_controls.control[pitch];
	_sys_id.trim[yaw] = _actuator_controls.control[yaw];
	_sys_id.trim[throttle] = _actuator_controls.control[throttle];
	return;
}

void msg_to_gazebo(bool s)
{
	int _state_sub_fd = orb_subscribe(ORB_ID(setstate));
	/* limit the update rate to 100 Hz */
	orb_set_interval(_state_sub_fd, 100);
	orb_copy(ORB_ID(setstate), _state_sub_fd, &_setstate);
	_setstate.start = s;
	orb_advert_t _setstate_pub = orb_advertise(ORB_ID(setstate), &_setstate);
	orb_publish(ORB_ID(setstate), _setstate_pub, &_setstate);
	PX4_INFO("#sysID Sent start to SETSTATE, %s", s ? "true" : "false");
}
