/** For implementation for Sysid **/
#include <drivers/drv_hrt.h>

#include <uORB/Publication.hpp>
#include <uORB/PublicationMulti.hpp>
#include <uORB/Subscription.hpp>
#include <uORB/SubscriptionCallback.hpp>

#include <uORB/topics/sys_id.h>
#include <uORB/topics/vehicle_acceleration.h>
#include <uORB/topics/setstate.h>
#include <uORB/topics/vehicle_angular_acceleration.h>
#include <uORB/topics/vehicle_attitude.h>
#include <uORB/topics/vehicle_angular_velocity.h>
#include <uORB/topics/vehicle_local_position.h>
#include <uORB/topics/actuator_controls.h>


class SystemIdentification
{
public:
	/** For implementation for Sysid **/
	uORB::Subscription _sys_id_sub{ORB_ID(sys_id)};
	uORB::Subscription _setstate_sub{ORB_ID(setstate)};
	uORB::Subscription _vehicle_acceleration_sub{ORB_ID(vehicle_acceleration)};
	uORB::Subscription _vehicle_angular_acceleration_sub{ORB_ID(vehicle_angular_acceleration)};
	uORB::Subscription _vehicle_rates_sub{ORB_ID(vehicle_angular_velocity)};
	uORB::Subscription _local_pos_sub{ORB_ID(vehicle_local_position)};		/**< local position subscription */

	sys_id_s 				_sys_id{};
	setstate_s 				_setstate{};
	vehicle_acceleration_s 			_vehicle_acceleration{};
	vehicle_attitude_s 			_vehicle_attitude{};
	vehicle_angular_velocity_s 		_vehicle_angularRate{};
	vehicle_angular_acceleration_s 		_vehicle_angular_acceleration{};
	vehicle_local_position_s 		_vehicle_local{};

	uint time_previous;
	bool firstcheck;

	/* State Checks */
	bool accelCheck;
	bool velCheck;
	bool angVelCheck;
	bool angAccelCheck;
	bool eulerCheck;

	actuator_controls_s overwriteRPYTcontrols(actuator_controls_s _actuators_control)
	{
		// Subscription
		_sys_id_sub.update(&_sys_id);
		// Check for whether sys_id is being used
		if (!_sys_id.start_now) return _actuators_control;
		if (!firstcheck) return _actuators_control;

		int roll = actuator_controls_s::INDEX_ROLL;
		int pitch = actuator_controls_s::INDEX_PITCH;
		int yaw = actuator_controls_s::INDEX_YAW;
		int throttle = actuator_controls_s::INDEX_THROTTLE;

		// instantiate a new structure
		actuator_controls_s cruise_actuators_control = _actuators_control;
		// before fixing the deflections, the uav has to have the proper states in trim condition
		// fixing the deflections
		if (_sys_id.state == sys_id_s::TEST_LONG_STATE) {
			// cruise_actuators_control.control[pitch] = _sys_id.trim[pitch];
			// cruise_actuators_control.control[throttle] = _sys_id.trim[throttle];

		}
		else if (_sys_id.state == sys_id_s::TEST_LAT_STATE) {
			// cruise_actuators_control.control[roll] = _sys_id.trim[roll];
			// cruise_actuators_control.control[yaw] = _sys_id.trim[yaw];
		}

		// Return cruise actuator control if there is no activation of pulse for system ID
		if (!_sys_id.activate) return cruise_actuators_control;

		// Instantiate a new structure
		actuator_controls_s altered_actuators_control = cruise_actuators_control;
		// Take the time now in (ms)
		double now = hrt_absolute_time()/10e3;
		float current = (double)_sys_id.amplitude * sin(2*3.14*(1/(double)_sys_id.period)*(now - (double)_sys_id.start_time));

		if (_sys_id.debug) {
			PX4_INFO("#Start pulse @ %f ms from Start Time", now);
			_sys_id.debug = false;
		}

		if (_sys_id.sendthrottle) {
			altered_actuators_control.control[throttle] = current + _sys_id.trim[throttle];
		}
		if (_sys_id.sendroll) {
			altered_actuators_control.control[roll] = current + _sys_id.trim[roll];
		}
		if (_sys_id.sendpitch) {
			altered_actuators_control.control[pitch] = current + _sys_id.trim[pitch];
		}
		if (_sys_id.sendyaw) {
			altered_actuators_control.control[yaw] = current + _sys_id.trim[yaw];
		}


		// Check duration/period (ms)
		if (now - (double)_sys_id.start_time > (double)_sys_id.period){
			_sys_id.activate = false;
			orb_advert_t sys_id_pub = orb_advertise(ORB_ID(sys_id), &_sys_id);
			orb_publish(ORB_ID(sys_id), sys_id_pub, &_sys_id);
			PX4_INFO("#End pulse @ %f", now);
			PX4_INFO("#Exiting pulse-in-sim");

		}

		return altered_actuators_control;
	};

	ECL_ControlData overwriteECLcontrols(ECL_ControlData input, matrix::Eulerf euler)
	{
		float setpointForwardVel = _setstate.vx;

		// Rearrange quaternion format from gazebo to PX4
		float q[4] = {_setstate.q[3],_setstate.q[0],_setstate.q[1],_setstate.q[2]};
		matrix::Dcmf R = matrix::Quatf(q);
		const matrix::Eulerf euler_desired(R);
		float setpointRoll = euler_desired.phi(), setpointPitch = euler_desired.theta();
		// PX4_INFO("DesiredRoll(%f) DesiredPitch(%f)", (double)euler_desired.phi(), (double)euler_desired.theta());

		float Roll = setpointRoll;
		float Pitch = setpointPitch;
		float Yaw = euler.psi();

		_vehicle_rates_sub.update(&_vehicle_angularRate);

		// Change applied to the angular rates PID (NED,rad/s) implement P controller
		// float Kpattrate = 0.2;
		// float setpointRollRate = _setstate.avx, setpointPitchRate = _setstate.avy, setpointYawRate = _setstate.avz;
		// float RollRate = setpointRollRate + Kpattrate * (setpointRollRate - _vehicle_angularRate.xyz[0]);
		// float PitchRate = setpointPitchRate + Kpattrate * (setpointPitchRate - _vehicle_angularRate.xyz[1]);
		// float YawRate = setpointYawRate + Kpattrate * (setpointYawRate - _vehicle_angularRate.xyz[2]);

		float ForwardVel = setpointForwardVel;

		input.roll_setpoint = Roll;
		input.pitch_setpoint = Pitch;
		input.yaw_setpoint = Yaw;
		input.roll_rate_setpoint = 0;
		input.pitch_rate_setpoint = 0;
		input.yaw_rate_setpoint = 0;
		input.airspeed = ForwardVel;

		// PX4_INFO("Roll(%f) Pitch(%f) Yaw(%f) RollRate(%f) PitchRate(%f) YawRate(%f) ForwardVel(%f)",
		// 	(double)Roll, (double)Pitch, (double)Yaw, (double)RollRate, (double)PitchRate, (double)YawRate, (double)ForwardVel);
		return input;
	};

	bool checkvehiclestatus(matrix::Eulerf euler)
	{
			// Subscription to
		//	vehicle_acceleration_s
		//	vehicle_attitude_s
		//	vehicle_angular_velocity_s
		//	vehicle_local_position_s

		_vehicle_acceleration_sub.update(&_vehicle_acceleration);
		_vehicle_rates_sub.update(&_vehicle_angularRate);
		_local_pos_sub.update(&_vehicle_local);
		_vehicle_angular_acceleration_sub.update(&_vehicle_angular_acceleration);

		float yaw = euler.psi();
		// PX4_INFO("yaw : %f", (double)yaw);

		float q[4] = {_setstate.q[3],_setstate.q[0],_setstate.q[1],_setstate.q[2]};
		matrix::Dcmf R = matrix::Quatf(q);
		const matrix::Eulerf euler_desired(R);

		float inv = 1/(cos(yaw)*cos(yaw) - (-sin(yaw))*sin(yaw));
		float VX = (float)cos(yaw)*inv*_vehicle_local.vx + (float)sin(yaw)*inv*_vehicle_local.vy;
		float VY = -(float)sin(yaw)*inv*_vehicle_local.vx + (float)cos(yaw)*inv*_vehicle_local.vy;
		// PX4_INFO("Velocity = %f, %f",(double)VX,(double)VY);

		// Gazebo is in ENU, while PX4 is in NED, we will use NED convention for comparison
		// Check the various states

		double AccelerationD[3] = {_setstate.ay - _vehicle_acceleration.xyz[0],_setstate.ax - _vehicle_acceleration.xyz[1],-(_setstate.az) - _vehicle_acceleration.xyz[2]};
		// PX4_INFO("AccelerationD = %f, %f, %f",AccelerationD[0],AccelerationD[1],AccelerationD[2] - 9.81);
		double sqAccelerationD = sqrt(pow(AccelerationD[0],2)+pow(AccelerationD[1],2)+pow(AccelerationD[2]-9.81,2));

		double VelocityD[3] = {_setstate.vx - VX,_setstate.vy - VY,-(_setstate.vz) - _vehicle_local.vz};
		// PX4_INFO("VelocityD = %f, %f, %f",VelocityD[0],VelocityD[1],VelocityD[2]);
		double sqVelocityD = sqrt(pow(VelocityD[0],2)+pow(VelocityD[1],2)+pow(VelocityD[2],2));

		double AngularAccelerationD[3] = {_setstate.aay - _vehicle_angular_acceleration.xyz[0],_setstate.aax - _vehicle_angular_acceleration.xyz[1],-(_setstate.aaz) - _vehicle_angular_acceleration.xyz[2]};
		// PX4_INFO("AngularAccelerationD = %f, %f, %f",AngularAccelerationD[0],AngularAccelerationD[1],AngularAccelerationD[2]);
		double sqAngularAccelerationD = sqrt(pow(AngularAccelerationD[0],2)+pow(AngularAccelerationD[1],2)+pow(AngularAccelerationD[2],2));

		double AngularVelocityD[3] = {_setstate.avy - _vehicle_angularRate.xyz[0],_setstate.avx - _vehicle_angularRate.xyz[1],-(_setstate.avz) - _vehicle_angularRate.xyz[2]};
		// PX4_INFO("AngularAccelerationD = %f, %f, %f",AngularAccelerationD[0],AngularAccelerationD[1],AngularAccelerationD[2]);
		double sqAngularVelocityD = sqrt(pow(AngularVelocityD[0],2)+pow(AngularVelocityD[1],2)+pow(AngularVelocityD[2],2));

		double EulerRollPitch[2] = {euler_desired.phi() - euler.phi(), euler_desired.theta() - euler.theta()};
		double sqEulerD = sqrt(pow(EulerRollPitch[0],2)+pow(EulerRollPitch[1],2));

		double sqthreshold = 1;
		double eulerSqthreshold = 0.2;
		// PX4_INFO("Acc(%s) Vel(%s) aVel(%s) aAcc(%s)",
		// 	sqAccelerationD < sqthreshold ? "pass" : "fail", sqVelocityD < sqthreshold ? "pass" : "fail", sqAngularVelocityD < sqthreshold ? "pass" : "fail", sqAngularAccelerationD < sqthreshold ? "pass" : "fail");

		accelCheck = sqAccelerationD < sqthreshold;
		velCheck = sqVelocityD < sqthreshold;
		angVelCheck = sqAngularVelocityD < sqthreshold;
		angAccelCheck = sqAngularAccelerationD < sqthreshold;
		eulerCheck = sqEulerD < eulerSqthreshold;

		if (sqAccelerationD < sqthreshold && sqVelocityD < sqthreshold && sqAngularVelocityD < sqthreshold && sqAngularAccelerationD < sqthreshold && sqEulerD < eulerSqthreshold) return true;
		else return false;

	};


protected:

};
