
#include <stdbool.h>
#include <stdint.h>
#include <math.h>

#include "DiveControl.h"
#include <systemlib/mavlink_log.h>
#include <mathlib/mathlib.h>

using namespace time_literals;

namespace divecontrol
{

DiveControl::DiveControl(ModuleParams *parent) :
	ModuleParams(parent),
	_state(),
	_initialized(false),
	_initialized_time(0),
	_flare_height(3),
	_desired_bearing(0),
	_virtual_wp(0,0),
	_lineup_wp(0,0),
	_dive_wp(0,0),
	_curr_wp(0,0)

{
}
void DiveControl::init(const hrt_abstime &now,
	double current_lat,
	double current_lon,
	double altitude,
	float flare_param,
	Vector2d virtual_wp,
	Vector2d lineup_wp,
	Vector2d dive_wp,
	Vector2d curr_wp,
	float bearing_wp)
{
	_state = DiveControlState::DCTRL_INITIALISE;
	_initialized_time = (float)now;
	_start_wp = Vector2d (current_lat, current_lon);
	_flare_height = flare_param;
	_altitude = altitude;
	_virtual_wp = virtual_wp;
	_lineup_wp = lineup_wp;
	_dive_wp = dive_wp;
	_curr_wp = curr_wp;
	_desired_bearing = bearing_wp;
	fixRoll = false;
	_initialized = true;
	AfterDubinNav = false;
	process_start_time = (float)(now)/1000;
}

void DiveControl::update(const hrt_abstime &now,
	float airspeed,
	float current_alt,
	double current_lat,
	double current_lon,
	orb_advert_t *mavlink_log_pub,
	float _pitch,
	float _roll,
	float _thrust,
	float _bearing,
	Vector2f ground_speed)
{
	float _getpitch = _pitch * (float)(180 / 3.1415);
	float _alt_diff = current_alt - getAltitudesetpoint(current_alt);
	float time_now_ms = (float)(now)/1000;
	/* Check the time from start time */
	checkStartTime(mavlink_log_pub, time_now_ms);

	switch (_state) {
		case DiveControlState::DCTRL_INITIALISE:
		{
			/* Kick out the drone from any spiral/loiter maneuver, issue a 0 roll and monitor the roll response */
			_vehicle_command_sub.update(&_vehicle_command);
			int n = 5;
			/* issue a 0 roll for n seconds*/
			bool istrue = time_now_ms - process_start_time > n * (float)pow(10,3);
			if (istrue){
				printf("#DCTRL_INITIALISE issue success after %d seconds \n", n);
				_state = DiveControlState::DCTRL_ALIGN;
				start_time_init = false;
				printf("#DCTRL_INITIALISE Suceeded\n");
			}
			break;
		}

		case DiveControlState::DCTRL_ALIGN:
		{
			if (!AfterDubinNav)
			{
				/*
				* After kicking the drone from any loiter maneuver, we calculate the turn maneuver using dubins path
				*/

				/*
				* Finding LSL RSL LSR RSR using [1] Dubins path generation for a fixed wing UAV (DOI:10.1109/ICUAS.2014.6842272)
				* https://www.researchgate.net/publication/269299811_Dubins_path_generation_for_a_fixed_wing_UAV
				*
				*/
				if (!_dubinspath.initialised){
					Vector2d Current;
					Current = Vector2d (current_lat, current_lon);
					float roll_max = (10.0/180.0*pi);
					/* We should set a parameter for rollmax*/
					printf("roll_max(%f)\n",(double)roll_max);

					/* Our final endpoint is the lineup_wp */
					_dubinspath.Setup(time_now_ms/1000, Current, _lineup_wp, _bearing, _desired_bearing, airspeed, roll_max, _param_dctrl_int.get(), (pi/2));

					printf("[DubinsPath] Complete Setup\n");
					float ShortestCircleDist = _dubinspath.UpdateCircleChoices();
					printf("ShortestCircleDist = %f\n", (double)ShortestCircleDist);
					switch(_dubinspath.sel){
						// Following NED convention
						case DubinsPathDecision::RightStraightRight:{
							printf("DubinsPathDecision::RightStraightRight\n");
							_dubinspath.dir(0) = 1;
							_dubinspath.dir(1) = 1;
							break;
						}
						case DubinsPathDecision::RightStraightLeft:{
							printf("DubinsPathDecision::RightStraightLeft\n");
							_dubinspath.dir(0) = 1;
							_dubinspath.dir(1) = -1;
							break;
						}
						case DubinsPathDecision::LeftStraightRight:{
							printf("DubinsPathDecision::LeftStraightRight\n");
							_dubinspath.dir(0) = -1;
							_dubinspath.dir(1) = 1;
							break;
						}
						case DubinsPathDecision::LeftStraightLeft:{
							printf("DubinsPathDecision::LeftStraightLeft\n");
							_dubinspath.dir(0) = -1;
							_dubinspath.dir(1) = -1;
							break;
						}
						default:{
							printf("Error No Path Found\n");
							break;
						}
					}
					_dubinspath.PathCalculation();
					printf("[Dubins Path] Initialised, Path Duration %f\n", (double)(_dubinspath.PathSize * _dubinspath.timeint)); // in seconds
					printf("[Dive -> Dubins Path] Finished Initialization\n");
					dubin_prev_time = time_now_ms/1000;
				}

				/* Only when it is the end of the planned path, then we can */
				/* CheckEndCallback will be true at this time (time_now_ms - process_start_time - _dubinspath.TotalPathTime*1000) */
				if (_dubinspath.CheckEndCallback(time_now_ms/1000))
				{

					printf("#AfterDubinNav issued \n");
					AfterDubinNav = true;
					break;
				}

				/* We have to first check the publish rates */
				float tdiff = (float)(now)/powf(10,6) - dubin_prev_time;
				// printf("[Dubins Path] tdiff %lf \n ", (double)tdiff);
				if (tdiff < _param_dctrl_int.get())
					break;

				int idx = _dubinspath.PathIndexCallback(time_now_ms/1000);
				// printf("#Dubin idx %d \n", idx);

				dubin_path_desired = _dubinspath.path[idx];

				printf("#Dubin path \n %lf %lf \n ", idx, dubin_path_desired(0), dubin_path_desired(1));
				// _l1_control.navigate_waypoints(Vector2d (current_lat, current_lon), _dubinspath.path[idx], Vector2d (current_lat, current_lon), ground_speed);

				/*
				* We should not use the lyapunov nonlinear controller (not working yet) since L1 works
				*/

				// Vector2d Current = Vector2d(current_lat, current_lon);
				// float getDubinRoll = _dubinspath.rollController(Current, _bearing, 0.01, 0.1, time_now_ms/1000);
				// printf("[Dubins] Index %d\n", _dubinspath.PathIndexCallback(time_now_ms/1000));

				dubin_prev_time = time_now_ms/1000;
				break;
			}

			/*
			* We do one last check to see whether the drone has aligned, if not we have to turn the drone around and restart
			*/
			float distance = get_distance_to_next_waypoint(current_lat, current_lon, _lineup_wp(0), _lineup_wp(1));
			float _bearing_diff = fabsf(_bearing - _desired_bearing);
			/* We should find the direction of motion vector */
			/* Direction of motion vector becomes very erratic due to closeness to the next point */
			float motion_heading_error = fabsf(_bearing - get_bearing_to_next_waypoint(current_lat, current_lon, _curr_wp(0), _curr_wp(1)));

			/* When [motion vector] and [heading] is aligned, activated roll constained to 0 */
			if (_bearing_diff <= (float)0.1 && motion_heading_error < (float)0.3) {fixRoll = true; }
			else
			{
				printf("#DCTRL_ALIGN [%.1fm %.1frad %.1ferror fixedRoll(%s) Roll(%.1f)]\n",
						(double)distance,
						(double)_bearing_diff,
						(double)motion_heading_error,
						fixRoll ? "t" : "f",
						(double)_roll);
				fixRoll = false;
			}

			/* Move on to DCTRL_LINEUP only if the [bearing, waypoint] is at the [desired_bearing, lineupwp]*/
			/* When [motion vector], [heading] and [distance] is aligned*/
			if (distance > 5 && _bearing_diff > (float)0.05) {
				if ((float)now - process_start_time > (float)pow(10,6))
				{
					printf("#DCTRL_ALIGN [%.1fm %.1frad %.1ferror fixedRoll(%s) Roll(%.1f)]\n",
						(double)distance,
						(double)_bearing_diff,
						(double)motion_heading_error,
						fixRoll ? "t" : "f",
						(double)_roll);
					process_start_time = now;
				}
			}
			else {
				mavlink_log_info(mavlink_log_pub, "	#DCTRL_ALIGN alt suceeded");
				mavlink_log_info(mavlink_log_pub, "	#Init DCTRL_LINEUP");
				_state = DiveControlState::DCTRL_LINEUP;
				start_time_init = false;
			}
			break;
		}

		case DiveControlState::DCTRL_LINEUP:
		{
			fixRoll = true;
			/* Move on to DCTRL_DIVE only if the [bearing, waypoint] is at the [desired_bearing, divewp]*/
			float distance = get_distance_to_next_waypoint(current_lat, current_lon, _dive_wp(0), _dive_wp(1));
			float _bearing_diff = fabsf(_bearing - _desired_bearing);
			if (distance > 5 || _bearing_diff > (float)0.05) {
				if ((float)now - process_start_time > (float)pow(10,6))
				{
					printf("#DCTRL_LINEUP [%.1fm %.1frad fixedRoll(%s) Roll(%.1f)]\n",
						(double)distance,
						(double)_bearing_diff,
						fixRoll ? "t" : "f",
						(double)_roll);
					process_start_time = now;
				}
			}
			else {
				mavlink_log_info(mavlink_log_pub, "	#DCTRL_LINEUP alt suceeded");
				mavlink_log_info(mavlink_log_pub, "	#Init DCTRL_DIVE");
				_state = DiveControlState::DCTRL_DIVE;
				start_time_init = false;
			}
			break;
		}

		case DiveControlState::DCTRL_DIVE:
		{
			/* Move on to DCTRL_FLARE only if the [altitude] is at the [flaring altitude]*/
			if (current_alt > getAltitudesetpoint(current_alt)) {
				if ((float)now - process_start_time > (float)pow(10,6))
				{
					printf("#DCTRL_DIVE alt=%.1f:%.1f; p=%.1fdeg; airspd=%.1f; thr=%.3f\n",
						(double)_alt_diff,
						(double)getAltitudesetpoint(current_alt),
						(double)_getpitch,
						(double)airspeed,
						(double)_thrust);
					process_start_time = now;
				}
			}
			else {
				mavlink_log_info(mavlink_log_pub, "	#DCTRL_DIVE alt suceeded");
				mavlink_log_info(mavlink_log_pub, "	#Init DCTRL_FLARE");
				_state = DiveControlState::DCTRL_FLARE;
				start_time_init = false;
			}
			break;
		}
		case DiveControlState::DCTRL_FLARE:
		{
			/* Move on to DCTRL_FLARE only if the [altitude] is at the [0]*/
			if (current_alt > getAltitudesetpoint(current_alt)) {
				if ((int)_prev_alt_diff != (int)_alt_diff){
					if ((float)now - process_start_time > (float)pow(10,6))
					{
						printf("#DCTRL_FLARE alt=%.1f:%.1f; p=%.1fdeg; airspd=%.1f; thr=%.3f\n",
							(double)_alt_diff,
							(double)getAltitudesetpoint(current_alt),
							(double)_getpitch,
							(double)airspeed,
							(double)_thrust);
						process_start_time = now;
					}
				}
			}
			else {
				mavlink_log_info(mavlink_log_pub, "	#DCTRL_FLARE alt suceeded");
				mavlink_log_info(mavlink_log_pub, "	#Init DCTRL_LAND");
				_state = DiveControlState::DCTRL_LAND;
				start_time_init = false;
			}
			break;
		}
		case DiveControlState::DCTRL_LAND:
		{
			mavlink_log_info(mavlink_log_pub, "#Its the endgame now");
			endPublish(mavlink_log_pub, now);
			break;
		}

		default:
			break;
	}
}

// leave the dive loop send command back into loiter
void
DiveControl::endPublish(orb_advert_t *mavlink_log_pub, const hrt_abstime &now)
{
	_initialized = false;
	float end_time = (float)(now)/1000000;
	float duration = ((float)now - _initialized_time)/1000000;
	mavlink_log_info(mavlink_log_pub, "#Dive Complete @ time %.3fs", (double)end_time);
	mavlink_log_info(mavlink_log_pub, "#Dive Duration = %.3fs", (double)duration);
	vehicle_command_s command_{};
	reset();
	// command_.command = vehicle_command_s::VEHICLE_CMD_NAV_LAST;
	/* We disarm the vehicle upon landing */
	command_.command = vehicle_command_s::VEHICLE_CMD_COMPONENT_ARM_DISARM;
	mavlink_log_info(mavlink_log_pub, "#Dive Command Switch: VEHICLE_CMD_NAV_LAST(%d)", command_.command);

	command_.target_system = _vehicle_command.target_system;
	command_.target_component = _vehicle_command.target_component;
	command_.timestamp = hrt_absolute_time();

	_vehicle_command_pub.publish(command_);
}

void DiveControl::reset()
{
	_initialized = false;
	fixRoll = 0;
	_state = DiveControlState::DCTRL_INITIALISE;
	AfterDubinNav = false;
}

void DiveControl::checkStartTime(orb_advert_t *mavlink_log_pub, float time)
{
	if (!start_time_init){
		process_start_time = time;
		start_time_init = true;
		mavlink_log_info(mavlink_log_pub, "#Sequence @ time %.3fs", (double)(time/1000000));
		return ;
	}
	else
		return ;
}

}
