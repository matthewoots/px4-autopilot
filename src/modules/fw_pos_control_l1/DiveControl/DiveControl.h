/**
 * @file DiveControl.h
 *
 * @author Woots
 */

#ifndef DIVECONTROL_H
#define DIVECONTROL_H

#include <stdbool.h>
#include <stdint.h>
#include <math.h>

#include <drivers/drv_hrt.h>
#include <px4_platform_common/module_params.h>
#include <systemlib/mavlink_log.h>
#include <mathlib/mathlib.h>
#include <matrix/math.hpp>
#include <lib/ecl/geo/geo.h>
#include <lib/l1/ECL_L1_Pos_Controller.hpp>
#include <lib/landing_slope/Landingslope.hpp>

#include <uORB/Publication.hpp>
#include <uORB/Subscription.hpp>
#include <uORB/topics/vehicle_command.h>
#include <uORB/topics/dive_control.h>
#include "dubinspath.h"

using matrix::Vector2d;
using matrix::Vector2f;
using namespace dubinspath;

namespace divecontrol
{

enum DiveControlState {
	DCTRL_INITIALISE = 0, /**< initialization of dive landing etc calculation of final waypoint*/
	DCTRL_ALIGN = 1, /**< align to diving runway which is the lineup */
	DCTRL_LINEUP = 2, /**< line up and prepare to dive, roll 0 */
	DCTRL_DIVE = 3, /**< dive action */
	DCTRL_FLARE = 4, /**< flare and drastically lower speed */
	DCTRL_LAND = 5 /**< land command issue and cut throttle */
};

class __EXPORT DiveControl : public ModuleParams
{
public:
	ECL_L1_Pos_Controller	_l1_control;
	DiveControl(ModuleParams *parent);
	~DiveControl() = default;

	DubinsPath _dubinspath;

	bool AfterDubinNav;

	void init(const hrt_abstime &now, double current_lat, double current_lon, double altitude, float flare_param,

	Vector2d virtual_wp, Vector2d lineup_wp, Vector2d _dive_wp, Vector2d curr_wp,float bearing_wp);

	void update(const hrt_abstime &now, float airspeed, float alt_agl, double current_lat, double current_lon,
		    orb_advert_t *mavlink_log_pub, float _pitch, float _roll, float _thrust, float _bearing, Vector2f ground_speed);

	DiveControlState getState() { return _state; }

	bool isInitialized() { return _initialized; }

	float getMinAirspeedScaling() { return _param_dctrl_airspd_scl.get(); }
	float getMaxSpeed() {return _param_fw_airspd_max.get() * _param_dctrl_airspd_scl.get();}
	float getMinSpeed() {return _param_fw_airspd_min.get() * _param_dctrl_airspd_scl.get();}

	Vector2d getLandingLatLon() { return Vector2d(_param_dctrl_lnd_lat.get(), _param_dctrl_lnd_lon.get()); }
	float getLandingHdg() { return _param_dctrl_lnd_hdg.get() * (float)(3.141593 / 180); }
	float getLineupDist() { return _param_dctrl_lnu_dst.get(); }

	float getAltitude(float current_alt) {
		// until dive, we should hold our altitude
		if (_state < DiveControlState::DCTRL_DIVE) {
			return current_alt;
		}
		else {
			return 0.0f;
		}
	}

	float getAltitudesetpoint(float current_alt) {
		// Until dive, we should hold our altitude setpoint as the same as our own altitude
		if (_state < DiveControlState::DCTRL_DIVE)
			return current_alt;
		/* During dive, we should track the descent line very closely, DCTRL_DIVE will give us dive_alt_track */
		/* We will end at _flare_height + _param_dctrl_alt_ths.get() */
		else if (_state == DiveControlState::DCTRL_DIVE)
			return (_flare_height + _param_dctrl_alt_ths.get());
		else
			return 0;
	}

	/* Returns Min pitch setpoint to use.*/
	float getMinPitchSeq(float variable_pitch_range){
		if (_state < DiveControlState::DCTRL_DIVE)
			return math::radians(_param_fw_p_lim_min.get());
		else if (_state == DiveControlState::DCTRL_DIVE)
			return math::radians(_param_dctrl_dsp_pitch.get() - variable_pitch_range);
		else if (_state > DiveControlState::DCTRL_DIVE)
			return math::radians(-_param_dctrl_dsp_pitch.get() - variable_pitch_range);
		else
			return 0;
	}

	/* Returns Max pitch setpoint to use.*/
	float getMaxPitchSeq(float variable_pitch_range){
		if (_state < DiveControlState::DCTRL_DIVE)
			return math::radians(_param_fw_p_lim_max.get());
		else if (_state == DiveControlState::DCTRL_DIVE)
			return math::radians(_param_dctrl_dsp_pitch.get() + variable_pitch_range);
		else if (_state > DiveControlState::DCTRL_DIVE)
			return math::radians(-_param_dctrl_dsp_pitch.get() + variable_pitch_range);
		else
			return 0;
	}

	/*
	* Returns the Desired Airspeed of the FW
	*/
	float getSpeed(float target_airspeed)
	{
		if (_state <= DiveControlState::DCTRL_DIVE)
			return target_airspeed;
		else
			return 0;
	}

	/*
	* Returns true as long as we're below navigation altitude
	*/
	bool controlYaw()
	{
		// Keep controlling yaw directly until we finish!
		return _state < DiveControlState::DCTRL_LAND;
	}

	float getThrust(float tecsThrust, float throttle_min)
	{
		if (_state == DiveControlState::DCTRL_ALIGN)
			return throttle_min * 0.1f;
		return tecsThrust;
	}

	int getStage() {return _state; }

	Vector2d getVirtual() {return _virtual_wp; }
	Vector2d getLineup()  {return _lineup_wp; }
	float getDesiredHdg() {return _desired_bearing;}
	bool getIsRollFixed() {return fixRoll;}

	void checkStartTime(orb_advert_t *mavlink_log_pub, float time);

	void endPublish(orb_advert_t *mavlink_log_pub, const hrt_abstime &now);

	float _prev_alt_diff;
	int spdPrev;
	double _altitude;
	float process_start_time;
	bool start_time_init;
	Vector2d init_pos;
	Vector2d dubin_path_desired;
	float dubin_prev_time;

	void reset();
	vehicle_command_s	_vehicle_command {};

	uORB::Subscription _vehicle_command_sub{ORB_ID(vehicle_command)};
	uORB::Publication<vehicle_command_s> _vehicle_command_pub{ORB_ID(vehicle_command)};						///< Vehicle command pub

private:
	/** state variables **/
	DiveControlState _state{DCTRL_INITIALISE};
	bool _initialized{false};
	hrt_abstime _initialized_time{0};
	float _flare_height{0};
	float _desired_bearing{0};
	Vector2d _virtual_wp{0,0};
	Vector2d _lineup_wp{0,0};
	Vector2d _dive_wp{0,0};
	Vector2d _curr_wp{0,0};

	matrix::Vector2d _start_wp;
	bool fixRoll;

	DEFINE_PARAMETERS(
		(ParamInt<px4::params::DCTRL_HDG>) _param_dctrl_hdg,
		(ParamFloat<px4::params::DCTRL_LND_LAT>) _param_dctrl_lnd_lat,
		(ParamFloat<px4::params::DCTRL_LND_LON>) _param_dctrl_lnd_lon,
		(ParamFloat<px4::params::DCTRL_LND_HDG>) _param_dctrl_lnd_hdg,
		(ParamFloat<px4::params::DCTRL_LNU_DST>) _param_dctrl_lnu_dst,
		(ParamFloat<px4::params::DCTRL_MAX_THR>) _param_dctrl_max_thr,
		(ParamFloat<px4::params::DCTRL_DSP_PITCH>) _param_dctrl_dsp_pitch,
		(ParamFloat<px4::params::DCTRL_VAR_ROLL>) _param_dctrl_var_roll,
		(ParamFloat<px4::params::DCTRL_AIRSPD_SCL>) _param_dctrl_airspd_scl,
		(ParamFloat<px4::params::DCTRL_ALT_THS>) _param_dctrl_alt_ths,
		(ParamFloat<px4::params::DCTRL_INT>) _param_dctrl_int,
		(ParamFloat<px4::params::DCTRL_AIRSPD>) _param_fw_airspd,

		(ParamFloat<px4::params::FW_P_LIM_MAX>) _param_fw_p_lim_max,
		(ParamFloat<px4::params::FW_P_LIM_MIN>) _param_fw_p_lim_min,
		(ParamFloat<px4::params::FW_AIRSPD_MIN>) _param_fw_airspd_min,
		(ParamFloat<px4::params::FW_AIRSPD_MAX>) _param_fw_airspd_max

	)

};

}

#endif // DIVECONTROL_H
