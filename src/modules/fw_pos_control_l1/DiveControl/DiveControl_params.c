/**
 * @file DiveControl_params.c
 *
 * Parameters for Dive and Land Maneuver
 *
 * @author Matthew <mwoo@nus.edu.sg>
 */

/**
 * Dive Control with landing gear
 *
 * @boolean
 * @group Dive Control
 */
PARAM_DEFINE_INT32(DCTRL_EN, 0);

/**
 * Specifies which heading should be held during runnway takeoff.
 *
 * 0: airframe heading, 1: heading towards takeoff waypoint
 *
 * @group Dive Control
 * @value 0 Airframe
 * @value 1 Waypoint
 * @min 0
 * @max 1
 *
 */
PARAM_DEFINE_INT32(DCTRL_HDG, 0);

/**
 * Predefined Latitude given to land after diving.
 *
 * @min -200.0
 * @max 200.0
 * @decimal 8
 * @increment 0.000001
 * @group Dive Control
 */
PARAM_DEFINE_FLOAT(DCTRL_LND_LAT, 1.331178);

/**
 * Predefined Longitude given to land after diving.
 *
 * @min -200.0
 * @max 200.0
 * @decimal 8
 * @increment 0.000001
 * @group Dive Control
 */
PARAM_DEFINE_FLOAT(DCTRL_LND_LON, 103.783269);

/**
 * Predefined Heading given to land.
 *
 * @unit deg
 * @min -180.0
 * @max 180.0
 * @decimal 8
 * @group Dive Control
 */
PARAM_DEFINE_FLOAT(DCTRL_LND_HDG, 0.0);

/**
 * Predefined Lineup Distance given to get heading before diving.
 *
 * @unit m
 * @min 5.0
 * @max 50.0
 * @decimal 3
 * @group Dive Control
 */
PARAM_DEFINE_FLOAT(DCTRL_LNU_DST, 50.0);

/**
 * Max throttle during runway takeoff.
 * (Can be used to test taxi on runway)
 *
 * @unit norm
 * @min 0.0
 * @max 1.0
 * @decimal 2
 * @increment 0.01
 * @group Dive Control
 */
PARAM_DEFINE_FLOAT(DCTRL_MAX_THR, 1.0);

/**
 * Pitch setpoint for dive. (threshold from max & min)
 *
 * @unit deg
 * @min -30.0
 * @max 30.0
 * @decimal 1
 * @increment 0.5
 * @group Dive Control
 */
PARAM_DEFINE_FLOAT(DCTRL_DSP_PITCH, -25.0);

/**
 * Threshold for roll during dive, flare and land sequences.
 * Roll is limited during dive, flare and land sequences to ensure enough lift and prevents aggressive
 * navigation before we land.
 *
 * @unit rad
 * @min 0.0
 * @max 3.14
 * @decimal 1
 * @increment 0.5
 * @group Dive Control
 */
PARAM_DEFINE_FLOAT(DCTRL_VAR_ROLL, 0.05);

/**
 * Min. airspeed scaling factor for dive.
 * Pitch up will be commanded when the following airspeed is reached:
 * FW_AIRSPD_MIN + ((FW_AIRSPD_MAX - FW_AIRSPD_MIN) / 2)  * DCTRL_AIRSPD_SCL
 *
 * @unit norm
 * @min 0.0
 * @max 2.0
 * @decimal 2
 * @increment 0.01
 * @group Dive Control
 */
PARAM_DEFINE_FLOAT(DCTRL_AIRSPD_SCL, 1.3);

/**
 *
 * @unit m
 * @min 0.0
 * @max 100.0
 * @decimal 1
 * @increment 1
 * @group Dive Control
 */
PARAM_DEFINE_FLOAT(DCTRL_AIRSPD, 13.0);

/**
 *
 * @unit m
 * @min 0.0
 * @max 5.0
 * @decimal 1
 * @increment 1
 * @group Dive Control
 */
PARAM_DEFINE_FLOAT(DCTRL_ALT_THS, 4.0);

/**
 *
 * @unit s
 * @min 0.01
 * @max 5.0
 * @decimal 3
 * @increment 0.01
 * @group Dive Control
 */
PARAM_DEFINE_FLOAT(DCTRL_INT, 0.2);
