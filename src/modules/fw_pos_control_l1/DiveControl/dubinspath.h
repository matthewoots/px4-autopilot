/**
 * @file dubinspath.hpp
 *
 * @author Woots
 *
 * Dubins Path Generation for a Fixed Wing UAV
 * https://ieeexplore-ieee-org.libproxy1.nus.edu.sg/stamp/stamp.jsp?tp=&arnumber=6842272
 *
 * 1. DUBINS AIRCRAFT MODEL
 * 2. PATH GENERATION (1. RSR, 2. RSL, 3. LSR, 4. LSL)
 * 3. PATH-FOLLOWING STRATEGY
 *
 */

#include <stdbool.h>
#include <stdint.h>
#include <math.h>

#include <drivers/drv_hrt.h>
#include <px4_platform_common/module_params.h>
#include <systemlib/mavlink_log.h>
#include <mathlib/mathlib.h>
#include <matrix/math.hpp>
#include <lib/ecl/geo/geo.h>

#include <uORB/Publication.hpp>
#include <uORB/Subscription.hpp>

using matrix::Vector2d;
using matrix::Vector2f;
using matrix::wrap_pi;
using matrix::wrap_2pi;

#define g 9.81
#define pi 3.141593
#define defaultSize 500
#define defaultTimeint 1
#define	dirR 1
#define	dirL -1

namespace dubinspath
{

enum DubinsPathDecision {
	RightStraightRight = 0,
	RightStraightLeft = 1,
	LeftStraightRight = 2,
	LeftStraightLeft = 3,
	NONE = 4
};

class DubinsPath
{
public:
	// ** Note that using ENU convention
	bool initialised = false;
	DubinsPathDecision sel;
	Vector2d path [defaultSize];
	float bearing [defaultSize];
	// float roll [defaultSize];
	float yaw_prev;
	float start_time;
	Vector2d dir;
	float TotalPathTime;

	// Parameters to initialised from main code
	int PathSize;
	float rollmax;
	float Vconst;
	float timeint;
	float init_bearing;
	float final_bearing;
	float ShortestCircleDist;
	float yaw_thres;
	int p1;
	int p2;

	Vector2d _Ci;
	Vector2d _Cf;
	Vector2d _start;
	Vector2d _final;

	void Reset()
	{
		Vconst = 0;
		rollmax = 0;
		sel = DubinsPathDecision::NONE;
		initialised = false;
		timeint = defaultTimeint;
		start_time = 0;
		PathSize = 0;
		ShortestCircleDist = 0;
		// resizef(defaultSize, roll);
		// resizef(defaultSize, bearing);
		// resizeVec(defaultSize, path);
	}

	void Setup(float now, Vector2d start, Vector2d final, float _start_bearing, float _final_bearing, float vconst, float _rollmax, float interval, float yaw_max)
	{
		// populate essential values
		start_time = now;
		_start = start;
		_final = final;
		init_bearing = _start_bearing;
		final_bearing = _final_bearing;
		Vconst = vconst;
		rollmax = _rollmax;
		timeint = interval;
		initialised = true;
		yaw_prev = _start_bearing;
		// Yaw_thres or yaw_max is not used over here so we can ignore
		yaw_thres = yaw_max;
	}

	Vector2d PathCallback(float time)
	{
		int idx = ceil((time - start_time) / timeint);
		return path[idx];
	}

	float BearingCallback(float time)
	{
		int idx = ceil((time - start_time) / timeint);
		return bearing[idx];
	}

	// float RollCallback(float time)
	// {
	// 	int idx = ceil((time - start_time) / timeint);
	// 	return roll[idx];
	// }

	bool CheckEndCallback(float time)
	{
		int idx = ceil((time - start_time) / timeint);
		return idx >= PathSize;
	}

	int PathIndexCallback(float time)
	{
		return ceil((time - start_time) / timeint);
	}

	void PathCalculation()
	{
		// initialise yaw_prev = bearing in init()
		// initialise roll_prev = 0 in init()
		// r * rad
		float ArcLength1 = MinTurnRadius() * (float)( fabs(getBearingDifferenceRad(init_bearing, LineSegmentBearing(_Ci, _Cf))));
		float ArcLength2 = MinTurnRadius() * (float)( fabs(getBearingDifferenceRad(LineSegmentBearing(_Ci, _Cf), final_bearing)));
		float TotalDistance = ArcLength1 + LineSegmentDistance(_Ci, _Cf) + ArcLength2;

		printf("Circle initial : %lf %lf \nCircle final : %lf %lf \n", (double)_Ci(0), (double)_Ci(1), (double)_Cf(0), (double)_Cf(0));
		printf("MinTurnRadius %f\n", (double)MinTurnRadius());
		printf("ArcLength1(getBearingDifference) %f rad \n", (double)fabs(getBearingDifference(init_bearing, LineSegmentBearing(_Ci, _Cf))));
		printf("ArcLength2(getBearingDifference) %f rad\n", (double)fabs(getBearingDifference(LineSegmentBearing(_Ci, _Cf), final_bearing)));
		printf("ArcLength1(Path) %f\n", (double)ArcLength1);
		printf("ArcLength2(Path) %f\n", (double)ArcLength2);
		printf("LineSegmentDistance(Path) %f\n", (double)LineSegmentDistance(_Ci, _Cf));

		printf("TotalDistance(Path) %f\n", (double)(TotalDistance));
		p1 = ArcLength1 / Vconst / timeint;
		p2 = p1 + LineSegmentDistance(_Ci, _Cf) / Vconst / timeint;
		printf("P1 (%d) P2 (%d)\n", p1, p2);

		PathSize = (int)floor(TotalDistance / Vconst / timeint);
		TotalPathTime = TotalDistance / Vconst;
		// resizeVec(PathSize+1, path);
		path[0](0) = _start(0);
		path[0](1) = _start(1);

		float frag = Vconst * timeint;
		float hr = GetHeadingRate((float)dir(0)) * timeint;
		printf("Heading Rate %f\n", (double)(hr));
		printf("FragmentDistance %f\n", (double)(frag));

		// RSR will be a clockwise to clockwise
		// RSL will be a clockwise to anticlockwise
		// LSR will be a anticlockwise to clockwise
		// LSL will be a anticlockwise to anticlockwise
		// float t = 0;
		float bearing_current;
		for (int p = 1; p < PathSize; p++)
		{
			// t = t + p * timeint;
			if (p <= p1){
				bearing_current = GetHeadingRate(rollmax * (float)dir(0)) * timeint + yaw_prev;

				float x = MinTurnRadius() * sin(GetHeadingRate(rollmax * (float)dir(0))) * timeint;
				float y = MinTurnRadius() * cos(GetHeadingRate(rollmax * (float)dir(0))) * timeint;
				float h = sqrt(pow(x,2) + pow(y,2));
				printf("h %lf\n",(double)h);
				float vect = getBearingRad(tan(x/y));

				waypoint_from_heading_and_distance(path[p-1](0), path[p-1](1), vect, h, &path[p](0), &path[p](1));
				// roll[p] = rollmax * (float)dir(0);
			}
			else if (p > p1 && p <= p2){
				bearing_current = yaw_prev;
				waypoint_from_heading_and_distance(path[p-1](0), path[p-1](1), bearing_current, frag,
					&path[p](0), &path[p](1));
				// roll[p] = 0;
			}
			else{
				bearing_current = GetHeadingRate(rollmax * (float)dir(1)) * timeint + yaw_prev;

				float x = MinTurnRadius() * sin(GetHeadingRate(rollmax * (float)dir(1))) * timeint;
				float y = MinTurnRadius() * cos(GetHeadingRate(rollmax * (float)dir(1))) * timeint;
				float h = sqrt(pow(x,2) + pow(y,2));
				float vect = getBearingRad(tan(x/y));

				waypoint_from_heading_and_distance(path[p-1](0), path[p-1](1), vect, h, &path[p](0), &path[p](1));
				// roll[p] = rollmax * (float)dir(1);
			}

			bearing[p] = bearing_current;
			printf("Bearing %d : %lf\n", p, (double)bearing[p]);
			// path[p](0) = path[p-1](0) + (double)(MinTurnRadius() * sinf(bearing_current));
			// path[p](1) = path[p-1](1) + (double)(MinTurnRadius() * cosf(bearing_current));
			// waypoint_from_heading_and_distance(path[p-1](0), path[p-1](1), bearing_current, MinTurnRadius(),
					// &path[p](0), &path[p](1));

			yaw_prev = bearing_current;
		}
		printf("Sizeof(Path) %d\n", PathSize);
		printf("Sizeof(Timeint) %f\n", (double)timeint);
	}

	float GetRollPD(float _roll_prev, float _roll, float kp, float kd)
	{
		float e = _roll - _roll_prev;
		return kp * e + kd * e;
	}

	float GetHeadingRate(float _roll)
	{
		return ((float)g/Vconst)*tanf(_roll);
	}

	Vector2d LatLon2XYConversion(Vector2d start, Vector2d final, float heading)
	{
		float tmp_dist = get_distance_to_next_waypoint(start(0), start(1), final(0), final(1));
		float tmp_bearing = get_bearing_to_next_waypoint(start(0), start(1), final(0), final(1));
		Vector2d xy;
		xy(0) = tmp_dist * sinf(tmp_bearing); // give x [lat]
		xy(1) = tmp_dist * cosf(tmp_bearing); // give y [long]
		return xy;
	}

	// Choosing whether to use 1. RSR, 2. RSL, 3. LSR, 4. LSL,
	float UpdateCircleChoices()
	{
		// y is long, x is lat
		Vector2d Ci [2];
		Vector2d Cf [2];
		float dist = pow(10,6);

		// initial centers of the right and left circles about the initial position and the initial heading using [rad] and [lat,lon]
		waypoint_from_heading_and_distance(_start(0), _start(1), wrap_pi((double)init_bearing + (pi/2)), MinTurnRadius(), &Ci[0](0), &Ci[0](1));
		waypoint_from_heading_and_distance(_start(0), _start(1), wrap_pi((double)init_bearing - (pi/2)), MinTurnRadius(), &Ci[1](0), &Ci[1](1));

		// final centers of the right and left circles about the initial position and the initial heading using [rad] and [lat,lon]
		waypoint_from_heading_and_distance(_final(0), _final(1), wrap_pi((double)final_bearing + (pi/2)), MinTurnRadius(), &Cf[0](0), &Cf[0](1));
		waypoint_from_heading_and_distance(_final(0), _final(1), wrap_pi((double)final_bearing - (pi/2)), MinTurnRadius(), &Cf[1](0), &Cf[1](1));

		printf("[CircleChoices] Start %f %f, %f %f\n",Ci[0](0),Ci[0](1),Ci[1](0),Ci[1](1));
		printf("[CircleChoices] Final %f %f, %f %f\n",Cf[0](0),Cf[0](1),Cf[1](0),Cf[1](1));
		// 1. RSR, 2. RSL, 3. LSR, 4. LSL
		int tmp_sel = 10;
		for (int i = 0; i < 2; i++){
			for (int f = 0; f < 2; f++){
				float tmp_dist = get_distance_to_next_waypoint(Ci[i](0), Ci[i](1), Cf[f](0), Cf[f](1));
				printf("[CircleChoices] %d : %f\n",(i*2)+f,(double)tmp_dist);
				if (tmp_dist < dist){
					tmp_sel = (i*2)+f;
					dist = tmp_dist;
					_Ci = Ci[i];
					_Cf = Cf[f];
				}
			}
		}
		if (tmp_sel == static_cast<int>(DubinsPathDecision::RightStraightRight)) sel = DubinsPathDecision::RightStraightRight;
		if (tmp_sel == static_cast<int>(DubinsPathDecision::RightStraightLeft)) sel = DubinsPathDecision::RightStraightLeft;
		if (tmp_sel == static_cast<int>(DubinsPathDecision::LeftStraightRight)) sel = DubinsPathDecision::LeftStraightRight;
		if (tmp_sel == static_cast<int>(DubinsPathDecision::LeftStraightLeft)) sel = DubinsPathDecision::LeftStraightLeft;

		return dist;
	}

	float MinTurnRadius()
	{
		// minumum turn distance for the UAV [Radius]
		return (powf(Vconst,2) / ((float)g * tanf(rollmax)));
	}

	float LineSegmentDistance(Vector2d start, Vector2d final)
	{
		return get_distance_to_next_waypoint(start(0), start(1), final(0), final(1));
	}

	float LineSegmentBearing(Vector2d start, Vector2d final)
	{
		return get_bearing_to_next_waypoint(start(0), start(1), final(0), final(1));
	}

		void resizef(int size, float* arr) {
		size_t newSize = size;
		float* newArr = new float[newSize];

		// memcpy( newArr, arr, size * sizeof(float) );

		size = newSize;
		delete [] arr;
		arr = newArr;
	}

	void resizeVec(int size, Vector2d* arr) {
		size_t newSize = size;
		Vector2d* newArr = new Vector2d[newSize];

		// memcpy( newArr, arr, size * sizeof(Vector2d) );

		size = newSize;
		delete [] arr;
		arr = newArr;
	}

	double getBearingDifference(double b1, double b2) {
		double r = fmod(b2 - b1, 360.0);
		if (r < -180.0)
			r += 360.0;
		if (r >= 180.0)
			r -= 360.0;
		return r;
	}

	double getBearingDifferenceRad(double b1, double b2) {
		double r = fmod(b2 - b1, 2*pi);
		if (r < -pi)
			r += 2*pi;
		if (r >= pi)
			r -= 2*pi;
		return r;
	}

	double getBearingRad(double b1) {
		double r = fmod(b1, 2*pi);
		if (r < -pi)
			r += 2*pi;
		if (r >= pi)
			r -= 2*pi;
		return r;
	}

	float rollController(Vector2d current, float yaw_current, float ks, float kw1, float time)
	{
		/* Lyapunov-based path following strategy
		*
		* ed = get_distance_to_next_waypoint(current(0), current(1), desired(0), desired(1)) * sin(get_bearing_to_next_waypoint(current(0), current(1), desired(0), desired(1)));
		* es = get_distance_to_next_waypoint(current(0), current(1), desired(0), desired(1)) * cos(get_bearing_to_next_waypoint(current(0), current(1), desired(0), desired(1)));
		* yaw_error = yaw_current - yaw_desired;
		* sigm_ed = - yaw_thres * ((exp(2 * ks * ed) - 1)/(exp(2 * ks * ed) + 1));
		* sigm_ed_dot = - ((4 * yaw_thres * ks * exp(2 * ks * ed))/pow((exp(2 * ks * ed) + 1),2));
		* cc = ifcurve ? (1 / MinTurnRadius()) : 0;
		* sprime = Vconst * cos(yaw_error) + ks * es;
		* beta = - cc * sprime - sigm_ed_dot * (Vconst * sin(yaw_error) - cc * es * sprime) + (Vconst * ed) * ((sin(yaw_error) - sin(sigm_ed))/(yaw_error - sigm_ed));
		* w = - beta - kw1 * (yaw_error - sigm_ed);
		*
		*/
		// int path_idx = PathIndexCallback(time);
		// bool ifcurve;
		// if (path_idx <= p1 || path_idx > p2) ifcurve = true;
		// else ifcurve = false;

		// // Vector2d desired = PathCallback(time);
		// // float yaw_desired = BearingCallback(time);
		// Vector2d desired = path[path_idx];
		// float yaw_desired = bearing[path_idx];

		// printf("[rollController] idx %d\n",PathIndexCallback(time));
		// printf("[rollController] Current [%f %f]\n", current(0), current(1));
		// printf("[rollController] Desired [%f %f]\n", desired(0), desired(1));
		// printf("[rollController] dist %f, heading %f\n",(double)get_distance_to_next_waypoint(current(0), current(1), desired(0), desired(1)), (double)get_bearing_to_next_waypoint(current(0), current(1), desired(0), desired(1)));
		// float ed = get_distance_to_next_waypoint(current(0), current(1), desired(0), desired(1)) * sin(get_bearing_to_next_waypoint(current(0), current(1), desired(0), desired(1)));
		// float es = get_distance_to_next_waypoint(current(0), current(1), desired(0), desired(1)) * cos(get_bearing_to_next_waypoint(current(0), current(1), desired(0), desired(1)));
		// float yaw_error = (float)getBearingDifferenceRad(yaw_desired, yaw_current);
		// printf("[rollController] sigm_num %f, sigm_den %f\n",(double)(expf(2 * ks * ed) - 1),(double)(expf(2 * ks * ed) + 1));
		// float sigm_ed = - yaw_thres * ((expf(2 * ks * ed) - 1)/(expf(2 * ks * ed) + 1));
		// float sigm_ed_dot = - ((4 * yaw_thres * ks * exp(2 * ks * ed))/powf((exp(2 * ks * ed) + 1),2));
		// float cc = ifcurve ? (1 / MinTurnRadius()) : 0;
		// float sprime = Vconst * cos(yaw_error) + ks * es;
		// float beta = - cc * sprime - sigm_ed_dot * (Vconst * sin(yaw_error) - cc * es * sprime) + (Vconst * ed) * ((sin(yaw_error) - sin(sigm_ed))/(yaw_error - sigm_ed));
		// float w = - beta - kw1 * (yaw_error - sigm_ed);
		// printf("[rollController] ed %f, es %f\n",(double)ed, (double)es);
		// printf("[rollController] yaw_error %f, sigm_ed %f, sigm_ed_dot %f\n",(double)yaw_error, (double)sigm_ed, (double)sigm_ed_dot);
		// printf("[rollController] cc %f, sprime %f, beta %f, w %f\n",(double)cc, (double)sprime, (double)beta, (double)w);
		// return atan2f(w * Vconst, g);
		return 0;
	}

};

}
