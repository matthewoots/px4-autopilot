We need to change from **SETPOINT_TYPE_LOITER** and **position_sp_type** needs to be **SETPOINT_TYPE_DIVE**
This is because while in loiter mode, there is conflicting command, since the previous and next setpoints will affect the navigation when trying to get to the dive point

---

1. **DiveControlState::DCTRL_INITIALISE**
