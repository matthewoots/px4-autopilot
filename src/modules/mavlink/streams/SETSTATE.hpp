/****************************************************************************
 *
 *   Copyright (c) 2020 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef SETSTATE_HPP
#define SETSTATE_HPP

#include <uORB/topics/setstate.h>

class MavlinkStreamSetstate : public MavlinkStream
{
public:
	static MavlinkStream *new_instance(Mavlink *mavlink) { return new MavlinkStreamSetstate(mavlink); }

	static constexpr const char *get_name_static() { return "SETSTATE"; }
	static constexpr uint16_t get_id_static() { return MAVLINK_MSG_ID_SETSTATE; }

	const char *get_name() const override { return get_name_static(); }
	uint16_t get_id() override { return get_id_static(); }

	unsigned get_size() override
	{
		static constexpr unsigned size_per_batch = MAVLINK_MSG_ID_SETSTATE_LEN + MAVLINK_NUM_NON_PAYLOAD_BYTES;
		return _setstate_sub.advertised() ? size_per_batch * _number_of_batches : 0;
	}

private:
	explicit MavlinkStreamSetstate(Mavlink *mavlink) : MavlinkStream(mavlink) {}

	uORB::Subscription _setstate_sub{ORB_ID(setstate)};
	uint8_t _number_of_batches{0};

	bool send() override
	{
		setstate_s setstate;

		if (_setstate_sub.update(&setstate)) {
			mavlink_setstate_t msg{};

			msg.timestamp = setstate.timestamp;
			msg.start = setstate.start;

			msg.x = setstate.x;
			msg.y = setstate.y;
			msg.z = setstate.z;

			msg.vx = setstate.vx;
			msg.vy = setstate.vy;
			msg.vz = setstate.vz;

			msg.ax = setstate.ax;
			msg.ay = setstate.ay;
			msg.az = setstate.az;

			msg.airspeed = setstate.airspeed;

			msg.avx = setstate.avx;
			msg.avy = setstate.avy;
			msg.avz = setstate.avz;

			msg.aax = setstate.aax;
			msg.aay = setstate.aay;
			msg.aaz = setstate.aaz;

			memcpy(msg.q, setstate.q, sizeof(setstate.q));
			msg.heading = setstate.heading;

			mavlink_msg_setstate_send_struct(_mavlink->get_channel(), &msg);
			return true;
		}

		return false;
	}
};

#endif // ESC_STATUS_HPP
