/****************************************************************************
 *
 *   Copyright (c) 2013-2015 PX4 Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name PX4 nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/**
 * @file simulator_params.c
 *
 * Parameters of software in the loop
 *
 * @author Mohamed Abdelkader <mohamedashraf123@gmail.com>
 * @author Matthew Woo <matthewoots@gmail.com>
 */

/**
 * Simulation Steps of Servos to PWM value
 *
 * This defines the servo step for the servos of the UAV, so as to mimic
 * actual servos and controllability
 *
 * @min 0.1
 * @max 3.0
 * @decimal 4
 * @increment 0.001
 * @group Simulator
 */
PARAM_DEFINE_FLOAT(SIM_SERVO_STEP, 2.0f);

/**
 * Simulation Max Programmable Swept Angle
 *
 * This defines the maximum swept angle for the UAV
 *
 * @min 0.1
 * @max 60.0
 * @decimal 4
 * @increment 0.001
 * @group Simulator
 */
PARAM_DEFINE_FLOAT(SIM_SWEPT_MAX, 30.0f);

/**
 * Simulation Max Programmable Retract Angle
 *
 * This defines the maximum retract angle for the UAV
 *
 * @min 0.1
 * @max 60.0
 * @decimal 4
 * @increment 0.001
 * @group Simulator
 */
PARAM_DEFINE_FLOAT(SIM_RETRT_MAX, 45.0f);

/**
 * Simulation left swept actuator
 *
 * @min 0
 * @max 15
 * @decimal 0
 * @increment 1
 * @group Simulator
 */
PARAM_DEFINE_INT32(SIM_LFT_SWEPT, 8);

/**
 * Simulation right swept actuator
 *
 * @min 0
 * @max 15
 * @decimal 0
 * @increment 1
 * @group Simulator
 */
PARAM_DEFINE_INT32(SIM_RGT_SWEPT, 9);

/**
 * Simulation left swept actuator
 *
 * @min 0
 * @max 15
 * @decimal 0
 * @increment 1
 * @group Simulator
 */
PARAM_DEFINE_INT32(SIM_LFT_RETRT, 10);

/**
 * Simulation right swept actuator
 *
 * @min 0
 * @max 15
 * @decimal 0
 * @increment 1
 * @group Simulator
 */
PARAM_DEFINE_INT32(SIM_RGT_RETRT, 11);
